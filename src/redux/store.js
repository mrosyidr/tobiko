import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import promise from 'redux-promise-middleware';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducer';

const middleware = composeWithDevTools(applyMiddleware(promise, thunk));

const store = createStore(reducer, middleware);

// const store = createStore(reducer, applyMiddleware(thunk));

export default store;
