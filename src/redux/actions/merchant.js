import axios from 'axios';
import Config from 'react-native-config';

import { getToken, showMessage } from '../../utils';

export const getMerchant = () => (dispatch) => {
  getToken().then((res) => {
    axios(Config.BASE_URL + '/api/merchant/order', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization': 'Bearer ' + res
      }
    }).then((response) => {
      dispatch({ type: 'SET_MERCHANT', value: response.data.data });
    }).catch((err) => {
      showMessage(err);
    });
  });
};
