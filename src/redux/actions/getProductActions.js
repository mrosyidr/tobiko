import getProduct from '../../api';

function fetchGetProduct(path){
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_GETALLPRODUCT_REQUEST'
    });

    try {
      const result = await getProduct(path);

      if (result.status === 200){
        dispatch({
          type: 'FETCH_GETALLPRODUCT_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_GETALLPRODUCTFAILED',
          payload: result.data
        });
      }
    } catch (error){
      dispatch({
        type: 'FETCH_GETALLPRODUCT_FAILED',
        error: error
      });
    }
  };
}

export default fetchGetProduct;
