import axios from 'axios';
import Config from 'react-native-config';

import { getToken, showMessage } from '../../utils';

export const getOrders = () => (dispatch) => {
  getToken().then((res) => {
    axios(Config.BASE_URL + '/api/merchant/order', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization': 'Bearer ' + res
      }
    }).then((response) => {
      dispatch({ type: 'SET_ORDER', value: response.data.data });
    }).catch((err) => {
      showMessage(err);
      //console.log('Errornya : ', err);
    });
  });
};

export const getPreparedOrders = () => (dispatch) => {
  getToken().then((res) => {
    axios(Config.BASE_URL + '/api/merchant/order', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization': 'Bearer ' + res
      }
    }).then((response) => {
      dispatch({ type: 'SET_PREPARED_ORDER', value: response.data.data });
    }).catch((err) => {
      showMessage(err);
      //console.log('Errornya : ',err);
    });
  });
};

export const getOrderDetails = (id) => (dispatch) => {
  getToken().then((res) => {
    // eslint-disable-next-line no-console
    console.log('res value:', res);
    axios(Config.BASE_URL + '/api/merchant/order/' + id, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization': 'Bearer ' + res
      }
    }).then((response) => {
      dispatch({ type: 'SET_ORDER_DETAIL', value: response.data.data });
    }).catch((err) => {
      showMessage(err);
      //console.log('Errornya : ',err);
    });
  });
};
