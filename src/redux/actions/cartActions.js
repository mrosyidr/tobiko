/* eslint-disable no-console */
// export function addToCart(id, quantity){
//   return async (dispatch) => {
//     dispatch({
//       type: 'ADD_TO_CART',
//       id,
//       quantity
//     });
//   };
// }

import { getCart } from '../../api';

function fetchCart(token){
  return async (dispatch) => {
    console.log('masuk ke fetch get cart');
    dispatch({
      type: 'FETCH_GETCART_REQUEST'
    });

    try {
      const result = await getCart(token);
      if (result.status === 200) {
        console.log('masuk sukses kah?');
        dispatch({
          type: 'FETCH_GETCART_SUCCESS',
          payload: result.data.data
        });
      }
      else {
        console.log('masuk failed kah?');
        dispatch({
          type: 'FETCH_GETCART_FAILED',
          payload: result.data.data
        });
      }
    } catch (error){
      console.log('error ini kah?', error);
      dispatch({
        type: 'FETCH_GETCART_FAILED',
        error: error
      });
    }
  };
}

export default fetchCart;
