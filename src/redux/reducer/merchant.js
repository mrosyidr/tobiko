const initMerchant = {
  merchant: []
};

export const merchantReducer = (state = initMerchant, action) => {
  if (action.type === 'SET_MERCHANT'){
    return {
      ...state,
      merchant: action.value
    };
  }
  return state;
};
