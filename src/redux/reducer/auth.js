const initStateRegister = {
  'email': '',
  'name': '',
  'password': ''
};

export const registerReducer = (state = initStateRegister, action) => {
  if (action.type === 'SET_REGISTER') {
    return {
      ...state,
      'email': action.value.email,
      'name': action.value.name,
      'password': action.value.password
    };
  }

  return state;
};
