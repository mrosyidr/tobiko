const initOrder = {
  orders: [],
  allOrder: [],
  prepared: [],
  onDelivery: [],
  completed: [],
  orderDetail: []
};

export const orderReducer = (state = initOrder, action) => {
  if (action.type === 'SET_ORDER') {
    return {
      ...state,
      orders: action.value
    };
  }
  if (action.type === 'SET_ALL_ORDER') {
    return {
      ...state,
      allorder: action.value
    };
  }
  if (action.type === 'SET_PREPARED_ORDER') {
    return {
      ...state,
      prepared: action.value
    };
  }
  if (action.type === 'SET_DELIVERY_ORDER') {
    return {
      ...state,
      onDelivery: action.value
    };
  }
  if (action.type === 'SET_COMPLETE_ORDER') {
    return {
      ...state,
      completed: action.value
    };
  }
  if (action.type === 'SET_ORDER_DETAIL') {
    return {
      ...state,
      orderDetail: action.value
    };
  }
  return state;
};
