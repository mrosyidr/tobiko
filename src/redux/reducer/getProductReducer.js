const defaultState = {
  payload: {},
  favorite: [],
  isLoading: false,
  error: {},
  total: 0
};

export default (state = defaultState, action = {}) => {
  switch (action.type){
    case 'FETCH_GETALLPRODUCT_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }

    case 'FETCH_GETALLPRODUCT_SUCCESS': {
      return {
        ...state,
        payload: action.payload,
        isLoading: false
      };
    }

    case 'FETCH_GETALLPRODUCT_FAILED': {
      return {
        ...state,
        payload: {},
        error: action.error,
        isLoading: false
      };
    }

    default:
      return state;
  }
};
