import { combineReducers } from 'redux';

import { registerReducer } from './auth';
import { globalReducer } from './global';
import getProductReducer from './getProductReducer';
import cartReducer from './cartReducers';
import getProfileReducer from './getProfileReducer';
import getProvinceReducer from './getProvinceReducer';
import getCityReducer from './getCityReducer';
import { orderReducer } from './order';
import { merchantReducer } from './merchant';

const reducer = combineReducers({ registerReducer,globalReducer,
  getProductReducer, cartReducer, getProfileReducer,getProvinceReducer, getCityReducer, orderReducer, merchantReducer });

export default reducer;
