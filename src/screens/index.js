import SplashScreen from './SplashScreen';
import SignIn from './SignIn';
import SignUp from './SignUp';
import SignUpAddress from './SignUpAddress';
import LoanPage from './LoanPage';
import ProfilePage from './ProfilePage';
import EditProfilePage from './EditProfilePage';
import MyBasketPage from './MyBasketPage';
import ForgotPassword from './ForgotPassword';
import TransferPayment from './TransferPayment';
import ConfirmOrderPage from './ConfirmOrderPage';
import MidTransPayment from './MidTransPayment';
import SuccessOrderLoan from './SuccessOrderLoan';
import MyOrdersPage from './MyOrdersPage';
import SuccessOrder from './SuccessOrder';
import MerchantHome from './MerchantHome';
import SignUpMerchant from './SignUpMerchant';
import SignUpMerchantAddress from './SignUpMerchantAddress';
import WalletPage from './WalletPage';
import DetailNewsPage from './DetailNewsPage';
import MerchantOrderDetail from './MerchantOrderDetail';
import MerchantInputResi from './MerchantInputResi';
import HomePage from './HomePage';
import DetailProduct from './DetailProduct';
import SearchPage from './SearchPage';
import AllNewsPage from './AllNewsPage';
import ProfileMerchant from './ProfileMerchant';
import MyOrderDetail from './MyOrderDetail';


export {
  SplashScreen,
  SignIn,
  SignUp,
  SignUpAddress,
  MerchantHome,
  LoanPage,
  ProfilePage,
  EditProfilePage,
  MyBasketPage,
  ForgotPassword,
  TransferPayment,
  ConfirmOrderPage,
  MidTransPayment,
  SuccessOrderLoan,
  MyOrdersPage,
  SuccessOrder,
  SignUpMerchant,
  SignUpMerchantAddress,
  WalletPage,
  DetailNewsPage,
  MerchantOrderDetail,
  MerchantInputResi,
  HomePage,
  DetailProduct,
  SearchPage,
  AllNewsPage,
  ProfileMerchant,
  MyOrderDetail
};
