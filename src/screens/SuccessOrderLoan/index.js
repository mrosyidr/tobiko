import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import imgSuccessOrderLoan from '../../../src/assets/Ilustration/SuccessOrderLoan.png';
import { CustomButton, Gap } from '../../components';

const SuccessOrderLoan = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.page}>
      <View style={styles.container}>
        <Image source={imgSuccessOrderLoan} style={{ height: Dimensions.get('window').height * 0.35, resizeMode: 'contain' }}/>
        <Text style={styles.mainText}>
          You’ve Made Order via Loan
        </Text>
        <Text style={styles.subText}>
          Please waiting for admin approval
        </Text>
        <Gap height={12} />
        <View width={Dimensions.get('window').width * 0.6} style={{ padding: 24 }}>
          <CustomButton text="Order Other Goods" onPress={() => navigation.replace('SearchPage')}/>
          <Gap height={12} />
          <CustomButton text="View My Orders" color="#8D92A3" onPress={() => navigation.replace('MyOrdersPage')} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SuccessOrderLoan;

const styles = StyleSheet.create({
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  mainText: { fontSize: 20, fontFamily: 'Poppins-Regular', color: '#020202', letterSpacing: -0.01 },
  subText: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#8D92A3' }
});
