/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { connect } from 'react-redux';
import axios from 'axios';
import Config from 'react-native-config';
import { Picker } from '@react-native-picker/picker';

import { CustomButton, Gap, Header, CustomTextInput } from '../../components';
import { useForm } from '../../utils';
import { setLoading, signUpAction } from '../../redux/actions';
import fetchGetProvince from '../../redux/actions/getProvinceActions';

import fetchGetCity from '../../redux/actions/getCityActions';


const SignUpAddress = (props) => {
  // const [refreshing, setRefreshing] = useState(true);
  const [listprovince, setListProvince] = useState([]);
  const [listcity, setListCity] = useState([]);
  // const [temp, setTemp] = useState('');


  const { navigation } = props;
  useEffect(() => {
    handleGetProvinceNoRedux();
    handleGetCityNoRedux();
  }, []);


  const handleGetProvinceNoRedux = async () => {
    await axios(Config.BASE_URL + '/api/data/province', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY
      }
    }).then((response) => { setListProvince(response.data.data); })
      .catch((error) => {
        console.log('Error di action getProvince', error);
      });
  };

  const handleGetCityNoRedux = async (province) => {
    await axios(Config.BASE_URL + '/api/data/city/' + province, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY
      }
    }).then((response) => {
      setListCity(response.data.data.cities);
      //mengecek province yang dipilih
    })
      .catch((error) => {
        console.log('Error di action getCity', error);
      });
  };

  const [form, setForm] = useForm({
    'phone': '',
    'address': '',
    'province': 1,
    'city': 1,
    'siup': ''
  });
  //mengubah state dengan memberikan type dan action valuenya..
  const dispatch = useDispatch();

  //mengambil state dalam redux
  const { registerReducer } = useSelector((state) => state);

  const onSubmit = () => {
    const data = {
      ...registerReducer,
      ...form
    };
    dispatch(setLoading(true));
    console.log('Data Register:', data);
    dispatch(signUpAction(data, navigation));
  };


  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.page}>
        <Header
          title="Address"
          subTitle="Make sure it’s valid"
          onBack={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <CustomTextInput
            label="Phone No."
            placeholder="Type your phone number"
            value={form.phone}
            keyboardType="phone-pad"
            onChangeText={(value) => setForm('phone', value)}
          />
          <Gap height={16} />
          <CustomTextInput
            label="Address"
            placeholder="Type your address"
            value={form.address}
            onChangeText={(value) => setForm('address', value)}
          />
          <Gap height={16} />

          <Text style={styles.label}>Province</Text>
          <View style={styles.input}>
            <Picker mode="dropdown" selectedValue={form.province}
              onValueChange={(value) => {
                // eslint-disable-next-line radix
                setForm('province', parseInt(value));
                handleGetCityNoRedux(value);
              }}>
              {
                listprovince.map((province) => {
                  //console.log('ini province object',province)
                  return (<Picker.Item label={province.name} value={province.id} key={province.id} />); //if you have a bunch of keys value pair
                })}
            </Picker>

          </View>
          <Gap height={16} />

          <Text style={styles.label}>City</Text>
          <View style={styles.input}>
            <Picker mode="dropdown" selectedValue={form.city}
              onValueChange={(value) => {
                // eslint-disable-next-line radix
                setForm('city', parseInt(value));
              }}>
              {listcity.map((city) => {
                return (<Picker.Item label={city.name} value={city.id} key={city.id} />); //if you have a bunch of keys value pair
              })}
            </Picker>
          </View>
          <Gap height={16} />
          <CustomTextInput
            label="SIUP"
            placeholder="Type your SIUP (optional)"
            value={form.siup}
            onChangeText={(value) => setForm('siup', value)}
          />
          <Gap height={24} />
          <CustomButton text="Sign Up Now" onPress={onSubmit} />
        </View>
      </View>
    </ScrollView>
  );
};


const styles = StyleSheet.create({
  scroll: { flexGrow: 1 },
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    marginTop: 24,
    flex: 1
  },
  label: { fontSize: 16, fontFamily: 'Poppins-Regular', color: '#020202' },
  input: {
    borderWidth: 1,
    borderColor: '#020202',
    borderRadius: 8,
    paddingHorizontal: 2,
    paddingVertical: 0
  }
});



function mapStateToProps(state) {
  return {
    getProvinceReducer: state.getProvinceReducer,
    getCityReducer: state.getCityReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchProvince: () => dispatch(fetchGetProvince()),
    dispatchCity: (id) => dispatch(fetchGetCity(id))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpAddress);
