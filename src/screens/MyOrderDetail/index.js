/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView, Dimensions, Alert, FlatList, SafeAreaView, LogBox } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { showMessage } from '../../utils';
import { CustomButton, Gap, Header, OrderProductList, OrderTimeline } from '../../components';
import getDetailOrder from '../../api/getDetailOrder/index';
import getProvince from '../../api/getProvince/index';
import getCity from '../../api/getCity/index';
import putFinishOrder from '../../api/putFinishOrder/index';

const MyOrderDetail = ({ route, navigation }) => {
//   const { navigation } = props;
  const { id } = route.params;
  const [product, setProduct] = useState([]);
  const [order, setOrder] = useState({});
  const [customer, setCustomer] = useState({});
  const [status, setStatus] = useState({});
  const [provinceId, setProvinceId] = useState(0);
  const [province, setProvince] = useState('');
  const [cityId, setCityId] = useState(0);
  const [city, setCity] = useState('');
  const [token, setToken] = useState('');

  useEffect(() => {
    processToken();
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);


  const processToken = async () => {
    try {
      const mytoken = await AsyncStorage.getItem('@token');
      //   const mytoken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiJkZWI0NGY2NC0zOWY3LTQzOWItYjM0Ni0yMDkzZjFjMjhmYWMiLCJpYXQiOjE2MTQ1ODE5MjksImV4cCI6MTYxNDY2ODMyOX0.hOdBKrTD6t7Se1cKA8XGwNUvCYkgEVNr6_2PWz2jH2E';
      if (mytoken){
        setToken(mytoken);
        getMyOrder(mytoken, id);
      }
    } catch (e) {
      showMessage('Please Login', 'warning');
    }
  };

  useEffect(() => {
    if (provinceId){
      getAllProvince();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[provinceId]);

  useEffect(() => {
    if (cityId){
      getAllCity(provinceId);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[cityId]);

  const getMyOrder = async(auth, idx) => {
    try {
      const res = await getDetailOrder(auth, idx);
      //   console.log('ini res : ', res.data.data.order.customer)
      setProduct(res.data.data.items);
      setOrder(res.data.data.order);
      setCustomer(res.data.data.order.customer);
      setStatus(res.data.data.order.workflow);
      setProvinceId(res.data.data.order.customer.provinceId);
      setCityId(res.data.data.order.customer.cityId);
    } catch (e) {
      showMessage('Bener ta?', 'warning');
    }
  };

  const handleConfirmFinish = () => {
    Alert.alert(
      'Alert',
      'Confirm Product Received?', [{
        text: 'Cancel',
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          finishOrder(token, order.id);
          navigation.replace('MyOrdersPage');
        }
      }], {
        cancelable: false
      }
    );
    return true;
  };

  const getAllProvince = async() => {
    try {
      const resp = await getProvince( '/api/data/province/');
      setProvince(resp.data.data.filter(obj => {return obj.id === provinceId;})[0].name);
    } catch (e) {
      throw e;
    }
  };

  const getAllCity = async() => {
    try {
      const resp = await getCity(provinceId);
      setCity(resp.data.data.cities.filter(obj => {return obj.id === cityId;})[0].name);
    } catch (e) {
      throw e;
    }
  };

  const finishOrder = async(tokenx, idx) => {
    try {
      const resp = await putFinishOrder(tokenx,idx);
      console.log('ini hasil putfinish : ', resp.data);
    } catch (e) {
    //   showMessage('Bener talh?', 'warning');
      throw e;
    }
  };

  const renderItemOrder = ({ item }) => {
    return (
      <OrderProductList
        name={item.product.name}
        price={item.total}
        banner={item.product.banner}
        quantity={item.quantity}
        OnPress={() => {
        //   moveToDetailProduct({ id: item.id });
        }}
      />
    );
  };


  return (
    <View style={styles.page}>
      <Header title="Your Order" subTitle="Awesomeness On the Way to You" onBack={() => navigation.goBack()} />
      <ScrollView style={styles.container}>
        <View style={{ flex: 1 }} showsVerticalScrollIndicator={false} >
          <View style={{ marginTop: 16, paddingHorizontal: 24 }}>
            <Text style={styles.section}>Item(s) Ordered</Text>
          </View>
          <SafeAreaView style={styles.order}>
            <FlatList
              // ref={flatListRef}
              data = {product}
              showsVerticalScrollIndicator={false}
              renderItem={renderItemOrder}
              keyExtractor={(item) => item.id.toString()}
              // refreshing={refreshing}
              // onRefresh={handleRefresh}
              // onEndReached={handleLoadMore}
            />
          </SafeAreaView>
        </View>
        <View style={{ marginTop: 16, paddingHorizontal: 24 }}>
          <Text style={styles.section}>Details Transaction</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <View>
              <Text style={styles.subSection}>
                {'Total Price'}
              </Text>
              <Text style={{ fontSize: 12, fontFamily: 'Poppins-Light', color: '#8D92A3' }}>
                {'(Include Delivery Fee)'}
              </Text>
            </View>
            <Text style={styles.totalPrice}>
              {/* {'Rp. '}{thousands(0/100*myCartTotal+0+myCartTotal)}{',-'} */}
              Rp {order.totalAmount}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'Status'}
            </Text>
            <Text style={styles.totalPrice}>
              {/* {'Rp. '}{thousands(0/100*myCartTotal+0+myCartTotal)}{',-'} */}
              {status.name}
            </Text>
          </View>
        </View>

        <View style={{ marginTop: 16, paddingHorizontal: 24 }}>
          <Text style={styles.section}>Deliver to</Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'Name'}
            </Text>
            <Text style={styles.section}>
              {customer.name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'Phone No.'}
            </Text>
            <Text style={styles.section}>
              {customer.phone}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'Address'}
            </Text>
            <Text style={styles.section}>
              {customer.address}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'City'}
            </Text>
            <Text style={styles.section}>
              {city}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 8
            }}>
            <Text style={styles.subSection}>
              {'Province'}
            </Text>
            <Text style={styles.section}>
              {/* {setProvince(allProvince.filter(obj => {return obj.id === customer.provinceId;})[0])} */}
              {/* { allProvince[0].name } */}
              {province}
            </Text>
          </View>
          <Gap height={20}/>
          <OrderTimeline id={status.id}/>
          <Gap height={16}/>
        </View>
      </ScrollView>

      {(status.id === 1 || status.id === 8) ?
        <View
          style={{
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
          <View width={Dimensions.get('window').width} style={{ padding: 24 }}>
            <CustomButton text="Order Other Product"
              onPress={() => navigation.navigate('HomePage')}
            />
          </View>
        </View>
        : status.id === 4 ? <View
          style={{
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
          <View width={Dimensions.get('window').width} style={{ padding: 24 }}>
            <CustomButton text="Confirm Receive Product"
              onPress={() => {
                handleConfirmFinish();
                //   finishOrder(token, order.id);
              }
              }
            />
          </View>
        </View>
          : <View
            style={{
              justifyContent: 'space-between',
              alignItems: 'center'
            }}>
            <View width={Dimensions.get('window').width} style={{ padding: 24 }}>
              <CustomButton text="Order Other Product"
                onPress={() => navigation.navigate('HomePage')}
              />
            </View>
          </View>
      }
    </View>
  );
};

export default MyOrderDetail;

const styles = StyleSheet.create({
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    marginTop: 24
  },
  address: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    marginTop: 24,
    flex: 1
  },
  order: {
    backgroundColor: '#FFFFFF',
    flex: 1
  },
  section: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#020202', fontWeight: 'bold' },
  subSection: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  orderItem: { fontSize: 16, fontFamily: 'Poppins-Medium', color: '#020202' },
  subOrderItem: { fontSize: 13, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  totalPrice: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#020202', fontWeight: 'bold' }
});
