/* eslint-disable no-console */
import React, { useEffect } from 'react';
import { View, SafeAreaView,Image } from 'react-native';
import { Text, Title, Subheading, Button, Snackbar } from 'react-native-paper';
import { TabBar, TabView, SceneMap } from 'react-native-tab-view';
import AsyncStorage from '@react-native-async-storage/async-storage';

import approveLoan from '../../api/putApproveLoan';
import rejectLoan from '../../api/putRejectLoan';

const LoanDetailPage = (props) => {

  let { route } = props;
  let params = route.params;
  let idloan = params.id;
  const initialLayout = { width: 400 };

  const [index,setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'approval', title: 'Approval' }
  ]);


  const [visible, setVisible] = React.useState(false);
  const ShowSnackbar = () => setVisible(!visible);
  const DismissSnackbar = () => setVisible(false);


  useEffect(()=>{
    console.log(idloan);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const HandleApproveLoan = async () => {
    let token = await AsyncStorage.getItem('@token');
    approveLoan(token,idloan).then((result)=>{
      console.log(result);
      ShowSnackbar();
    }).catch((err)=>{
      console.log(err);
    });
  };

  const HandleRejectLoan = async () => {
    let token = await AsyncStorage.getItem('@token');
    rejectLoan(token,idloan).then((result)=>{
      console.log(result);
      ShowSnackbar();
    }).catch((err)=>{
      console.log(err);
    });
  };

  const ApprovalRoute = () => {
    return (
      <View style={{ flex: 1,flexDirection: 'column',alignItems: 'flex-start' }}>
        <View style={{ marginTop: 15, flex: 2, flexDirection: 'row' }}>

          <Image source={{ uri: params.photo }} style={{ marginHorizontal: 3,width: 78,height: 78,flex: 2 }}/>
          <View style={{ marginVertical: 8, marginHorizontal: 20,flex: 5, flexDirection: 'column' }}>
            <Text style={{ fontSize: 18,color: '#222222',marginVertical: 2 }}>{params.name}</Text>
            <Text style={{ fontSize: 16,color: '#444444' }}>{params.installment}</Text>
          </View>


        </View>
        <Image source={{ uri: params.paymentProof }} style={{ marginHorizontal: 3,marginVertical: 30,width: 500,height: 700,flex: 9 }}/>
        <View style={{ marginTop: 15, flex: 2, flexDirection: 'row' }}>
          <View style={{ marginVertical: 8, marginHorizontal: 20,flex: 4, flexDirection: 'column' }}>
            <Text style={{ fontSize: 17,color: '#222222',marginVertical: 2 }}>Loan</Text>
            <Text style={{ fontSize: 14,color: '#444444' }}>{params.installment}</Text>
          </View>
          <Button mode="text" color="red"
            style={{
              fontSize: 15,
              marginVertical: 8,
              marginHorizontal: 10,
              flex: 2,
              textAlign: 'center',
              height: 44,
              justifyContent: 'center' }} onPress={()=>HandleRejectLoan()}>Reject
          </Button>
          <Button mode="text" color="green"
            style={{
              fontSize: 15,
              marginVertical: 8,
              marginHorizontal: 10,
              flex: 2,
              textAlign: 'center',
              height: 44,
              justifyContent: 'center'
            }} onPress={()=>HandleApproveLoan()}>Approve
          </Button>
        </View>
      </View>
    );
  };



  const renderTabBar = propstab => {
    return (
      <TabBar
        {...propstab}
        indicatorStyle={{ backgroundColor: 'orange' }}
        style={{ backgroundColor: 'white' }}
        renderLabel={({ route, focused, color }) => (
          <Text style={{ color: '#666666', fontSize: 15 }}>
            {route.title}
          </Text>
        )}
      />
    );
  };

  const renderScene = SceneMap({
    approval: ApprovalRoute
  });

  return (
    <SafeAreaView style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 5 }}>
      <View style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 10 }}>
        <Title style={{ fontSize: 30 }}>Administration</Title>
        <Subheading style={{ marginBottom: 20,color: '#777777' }}>You are now an admin</Subheading>
        <TabView
          renderTabBar={renderTabBar}
          navigationState={{ index, routes }}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={initialLayout}
          style={{ fontSize: 40 }}/>
        <Snackbar
          visible={visible}
          onDismiss={DismissSnackbar}>
						Success!
        </Snackbar>
      </View>
    </SafeAreaView>
  );

};

export default LoanDetailPage;


