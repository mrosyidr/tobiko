/* eslint-disable no-console */
import React from 'react';
import { SafeAreaView, StyleSheet, View } from 'react-native';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import Config from 'react-native-config';

import { CustomButton, Gap, Header, CustomTextInput } from '../../components';
//import {signInAction} from '../../redux/action/auth';
import { showMessage, useForm } from '../../utils';
import { setLoading } from '../../redux/actions';


const ForgotPassword = ({ navigation }) => {

  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    'email': ''
  });
  // const dispatch = useDispatch();

  //   const processToken = async () => {
  //     const token = await getToken();
  //     if (token) {
  //       navigation.navigate('HomePage')
  //     }
  //   }

  //   useEffect(() => {
  //     processToken()
  //   }, []);
  const handleForgot = async (form) => {
    console.log('form:',form);
    dispatch(setLoading(true));
    await axios(Config.BASE_URL + '/api/auth/forgot', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY
      },
      data: form
    }).then((response) => {
      showMessage(response.data.message, 'success');
      //storeData(response.data.data.token);
      dispatch(setLoading(false));
      navigation.replace('SignIn');
    }).catch((err) => {
      // handle error
      //console.log('Login is Failed:',err.response)
      showMessage('Failed:',err.response);
      dispatch(setLoading(false));
      //setLoading(false);
    });
  //navigation.replace('HomePage');
  };



  const onSubmit = (form) => {
    if (form.email){
      handleForgot(form);
    }
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={styles.page}>
        <Header title="Reset Password" subTitle="Reset your password" onBack={() => navigation.goBack()}/>
        <View style={styles.container}>
          <CustomTextInput
            label="Email Address"
            placeholder="Type your email address"
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={24} />
          <CustomButton text="Submit" onPress={() => onSubmit(form)} />
          <Gap height={12} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  page: { flex: 1 },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    marginTop: 24,
    flex: 1
  }
});
