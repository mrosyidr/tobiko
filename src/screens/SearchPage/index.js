import React, { useState, useEffect, useRef } from 'react';
import { FlatList, View, Text, TextInput } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

import { Header, ProductList } from '../../components';

import getSearchProduct from '../../api/getSearchProduct/index';

import styles from './styles';


function SearchPage(props){
  const { navigation } = props;
  const flatListRef = useRef();
  const [refreshing, setRefreshing] = useState(true);
  const [totalPage, setTotalPage] = useState(0);
  const [list, setList] = useState([]);
  const [word, setWord] = useState('');
  const [subTitle, setSubTitle] = useState('');
  const [page, setPage] = useState(1);

  useEffect(() => {
    handleGetProduct(word,page);
    setSubTitle('All Products');
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleGetProduct = async (kata,halaman) => {
    try {
      const res = await getSearchProduct(kata,halaman);
      setList(res.data.data.products);
      setTotalPage(res.data.data.total_pages);
      setRefreshing(false);
    } catch (e) {
      setList([]);
    }
  };

  const handleRefresh = () => {
    handleGetProduct(word,1);
  };

  const handleLoadMore = () => {
    if (totalPage > 1) {
      handleAddProduct(word,page);
    }
  };

  const handleAddProduct = async (kata,halaman) => {
    halaman = halaman + 1;
    try {
      const res = await getSearchProduct(kata,halaman);
      let listData = list;
      if (res.data.data.products.length === 0) {
        // alert('No More to Reload');
        setList(listData);
        setRefreshing(false);
        setPage(1);
      } else {
        let data = listData.concat(res.data.data.products);
        setList(data);
        setRefreshing(false);
        setPage(halaman);
      }
    } catch (e) {
      setRefreshing(false);
      throw e;
    }
  };

  const renderItemSearch = ({ item }) => {
    return (
      <ProductList
        name={item.name}
        banner={item.banner}
        averageRating={item.averageRating}
        price={item.price}
        OnPress={() => {
          moveToDetailProduct({ id: item.id });
        }}
      />
    );
  };

  const moveToDetailProduct = ({ id }) => {
    setList(list.map(function(o){
      o.quantity = 0;
      return o;
    }));
    navigation.navigate('DetailScreen', { id: list.filter(obj => {return obj.id === id;}) });
  };

  return (
    <View style={{ flex: 1 }}>
      <Header title="Tobiko" subTitle="One Stop B2B Solution by Telkomsel" />
      <View style={styles.searchBar}>
        <View style={styles.searchBarInside}>
          <Icons name="search" size={20} color="#979797" style={{ marginLeft: 13, marginTop: 17 }}/>
          <TextInput
            placeholder="Search Product"
            placeholderTextColor="#8E8E93"
            style={styles.searchBarTextInput}
            value={word}
            autoFocus={true}
            onChangeText={(val) => setWord(val)}
            onSubmitEditing={()=> {
              handleGetProduct(word,1);
              handleRefresh();
              setRefreshing(true);
              setSubTitle(`Search Result for ${word}`);
            }}
          />
        </View>
      </View>

      <View style={styles.subTitle}>
        <Text style={{ fontFamily: 'Poppins-Regular' }}>
          {subTitle}
        </Text>
      </View>

      <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
        {list.length > 0 ?
          <View>
            <FlatList
              ref={flatListRef}
              data = {list}
              showsVerticalScrollIndicator={false}
              renderItem={renderItemSearch}
              keyExtractor={(item) => item.id.toString()}
              refreshing={refreshing}
              onRefresh={handleRefresh}
              onEndReached={handleLoadMore}
            />
          </View>
          : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 16, fontFamily: 'Poppins-Medium', color: '#8D92A3', textAlign: 'center' }}>
                Sorry, no item contain keyword : {word}
            </Text>
          </View>
        }
      </View>
    </View>
  );
}

export default SearchPage;
