/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Image, ActivityIndicator, Alert } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { RadioButton } from 'react-native-paper';

import { CustomButton, Header } from '../../components';
import { getToken } from '../../utils';
import { getCart, getUserCity } from '../../api';

import styles from './styles';

const MyBasketPage = (props) => {
  const { navigation } = props;
  const [token, setToken] = useState('');

  const [subTotalPrice, setSubTotalPrice] = useState(0);
  const [totalWeight, setTotalWeight] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  const [merchantId, setMerchantId] = useState('');

  const [dataProductOrder, setDataProductOrder] = useState([]);

  const [isEmptyCart, setIsEmptyCart] = useState(false);
  const [isSingleMerchant, setIsSingleMerchant] = useState(true);
  const [merchantRadioBtn, setMerchantRadioBtn] = useState('');
  const [cityFromProfile, setCityFromProfile] = useState('');

  const [myCartArrObject, setMyCartArrObject] = useState([{
    merchantId: '',
    merchatnName: '',
    merchantTotal: 0,
    items: [{
      productId: 0,
      product: '',
      price: 0,
      banner: 'https://',
      qty: 0,
      total: 0,
      weight: 0,
      totalWeight: 0,
      isChecked: false
    }]
  }]);

  const [data, setData] = useState([{
    merchantId: '',
    merchatnName: '',
    merchantTotal: 0,
    items: [{
      productId: 0,
      product: '',
      banner: 'https://',
      total: 0,
      qty: 0,
      weight: 0,
      totalWeight: 0,
      isChecked: false
    }]
  }]);

  const [selectedProducts, setSelectedProducts] = useState([{
    productId: 0,
    product: '',
    price: 0,
    banner: 'https://',
    qty: 0,
    total: 0,
    weight: 0,
    totalWeight: 0,
    isChecked: false,
    merchantId: ''
  }]);

  function thousands(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  const authCheck = async () => {
    try {
      const myToken = await getToken();
      if (myToken) {
        setToken(myToken);
        await handleGetCartArrObject(myToken)
          .then((response) => {
            if (response.length > 0) {
              setMyCartArrObject(response);
              setData([{
                merchantId: '',
                merchatnName: '',
                merchantTotal: 0,
                items: [{
                  productId: 0,
                  product: '',
                  banner: 'https://',
                  total: 0,
                  qty: 0,
                  selected: false
                }]
              }]);
            }
          })
          .catch(err => console.log('error on get cart API:' + err));
      } else {
        navigation.replace('SignIn');
      }
    } catch (e) {
      console.log(e);
    }
  };

  const handleGetCartArrObject = async (tokenCart) => {
    try {
      const getCartArrObject = await getCart(tokenCart);
      return getCartArrObject;
    } catch (err) {
      console.log('error getting Cart Show API: ' + err);
    }
  };

  const handleGetCityFromProfile = async (tokenCity) => {
    const getCityFromProfileVal = await getUserCity(tokenCity);
    setCityFromProfile(getCityFromProfileVal);
  };

  const handleSelectedProducts = () => {
    const listSelectedArrTemp = [];
    const listSelectedMerchentIdTemp = [];
    data.forEach(item => {
      item.items.forEach(item2 => {
        if (item2.isChecked === true) {
          let tempObj = {
            productId: item2.productId,
            product: item2.product,
            price: item2.price,
            banner: item2.banner,
            qty: item2.qty,
            total: item2.total,
            weight: item2.weight,
            totalWeight: item2.totalWeight,
            isChecked: item2.isChecked,
            merchantId: item2.merchantId
          };

          setMerchantId(item.merchantId);
          listSelectedArrTemp.push(tempObj);
          listSelectedMerchentIdTemp.push(item.merchantId);

          if (listSelectedMerchentIdTemp.length > 0) {
            if (listSelectedMerchentIdTemp[0] === item.merchantId) {
              setIsSingleMerchant(true);
            }
            else {
              setIsSingleMerchant(false);
            }
          }
        }
      });
    });
    if (listSelectedArrTemp.length > 0) {
      setSelectedProducts(listSelectedArrTemp);
    }
    else {
      setSelectedProducts([{
        productId: 0,
        product: '',
        price: 0,
        banner: 'https://',
        qty: 0,
        total: 0,
        weight: 0,
        totalWeight: 0,
        isChecked: false,
        merchantId: ''
      }]);
    }
  };

  const onChangeValue = (itemSelected) => {
    const newData = data.map(item => {
      item.items.forEach(item2 => {
        (item2.productId === itemSelected.productId) ? item2.isChecked = !item2.isChecked : item2.isChecked = item2.isChecked;
      });
      return {
        ...item
      };
    });
    setData(newData);
    handleSelectedProducts();
  };

  const handleCheckout = () => {
    Alert.alert(
      'Checkout Now',
      'Confirm to checkout selected cart items?', [{
        text: 'Cancel',
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          onSubmit();
        }
      }], {
        cancelable: false
      }
    );
    return true;
  };

  const onSubmit = () => {
    navigation.replace('ConfirmOrderPage', {
      cityFromProfile, selectedProducts, merchantId, subTotalPrice, totalWeight, totalAmount, totalItems, dataProductOrder });
  };

  const calculatesubTotalPrice = () => {
    let subTotalPriceTemp = 0;
    if (data !== null && selectedProducts[0].product === '') {
      data.forEach(item => {
        item.items.forEach(item2 => {
          if (item2.isChecked === true) {
            subTotalPriceTemp += item2.total;
          }
        });
      });
      setSubTotalPrice(subTotalPriceTemp);
    }
    else {
      selectedProducts.forEach(item => {
        subTotalPriceTemp += item.total;
      });
      setSubTotalPrice(subTotalPriceTemp);
    }
  };

  const calculateTotalWeight = () => {
    let totalWeightTemp = 0;
    if (data !== null && selectedProducts[0].product === '') {
      data.forEach(item => {
        item.items.forEach(item2 => {
          if (item2.isChecked === true) {
            totalWeightTemp += item2.totalWeight;
          }
        });
      });
      setTotalWeight(totalWeightTemp);
    }
    else {
      selectedProducts.forEach(item => {
        totalWeightTemp += item.totalWeight;
      });
      setTotalWeight(totalWeightTemp);
    }
  };

  const calculateTotalAmount = () => {
    let totalAmountTemp = 0;
    if (data !== null && selectedProducts[0].product === '') {
      data.forEach(item => {
        item.items.forEach(item2 => {
          if (item2.isChecked === true) {
            totalAmountTemp += item2.total;
          }
        });
      });
      setTotalAmount(totalAmountTemp);
    }
    else {
      selectedProducts.forEach(item => {
        totalAmountTemp += item.total;
      });
      setTotalAmount(totalAmountTemp);
    }
  };

  const calculateTotalItems = () => {
    let totalItemsTemp = 0;
    if (data !== null && selectedProducts[0].product === '') {
      data.forEach(item => {
        item.items.forEach(item2 => {
          if (item2.isChecked === true) {
            totalItemsTemp += item2.qty;
          }
        });
      });
      setTotalItems(totalItemsTemp);
    }
    else {
      selectedProducts.forEach(item => {
        totalItemsTemp += item.qty;
      });
      setTotalItems(totalItemsTemp);
    }
  };

  const passDataOrder = () => {
    let dataProductTemp = [];
    if (data !== null && selectedProducts[0].product === '') {
      data.forEach(item => {
        item.items.forEach(item2 => {
          if (item2.isChecked === true) {
            let objTemp = {
              productId: item2.productId,
              quantity: item2.qty,
              price: item2.price,
              total: item2.total,
              totalWeight: item2.totalWeight
            };
            dataProductTemp.push(objTemp);
          }
        });
      });
      setDataProductOrder(dataProductTemp);
    }
    else {
      selectedProducts.forEach(item => {
        let objTemp = {
          productId: item.productId,
          quantity: item.qty,
          price: item.price,
          total: item.total,
          totalWeight: item.totalWeight
        };
        dataProductTemp.push(objTemp);
      });
      setDataProductOrder(dataProductTemp);
    }
  };

  useEffect(() => {
    authCheck();
    if (token !== '') {
      handleGetCityFromProfile(token);
    }
    setTimeout(() => {
      setIsEmptyCart(true);
    }, 5000);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (myCartArrObject) {
      setMerchantRadioBtn(myCartArrObject[0].merchantId);
      setData(myCartArrObject);
      if (myCartArrObject[0].merchantId !== '') {
        handleSelectedProducts();
        calculatesubTotalPrice();
        calculateTotalWeight();
        calculateTotalAmount();
        calculateTotalItems();
        passDataOrder();
        if (merchantId === '') {
          setMerchantId(data[0].merchantId);
        }
        if (cityFromProfile === '') {
          handleGetCityFromProfile(token);
        }
        if (isEmptyCart === true) {
          setIsEmptyCart(false);
        }
      }
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [myCartArrObject, data]);

  useEffect(() => {
    return () => {
      console.log('-- MyBasketPage components cleanup and unmounted');
    };
  }, []);

  // temp: for debug purposes
  // console.log('outer part: ');
  // console.log(isEmptyCart);

  return (
    <View style={styles.page}>
      <Header title="My Cart" subTitle="Let’s check it out" onBack={() => navigation.reset({ index: 0, routes: [{ name: 'HomePage' }] })} />
      <View style={styles.container}>
        {
          (data[0].merchantId !== '') ?
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false} >
              {
                data.map((item, index) => {
                  return (
                    <RadioButton.Group key={index} onValueChange={newValue => setMerchantRadioBtn(newValue)} value={merchantRadioBtn}>
                      <View key={index}>
                        <View style={[styles.rowCenterBetween, { marginTop: 16 }]}>
                          <View style={styles.rowCenter}>
                            <View style={styles.rowCenter}>
                              <Text style={styles.orderItem}>
                                {item.merchatnName}
                              </Text>
                            </View>
                          </View>
                          <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.subTotalItem}>{'Rp. '}{thousands(item.merchantTotal)}{',-'}</Text>
                          </View>
                        </View>
                        <View>
                          {
                            item.items.map((item2, index2) => {
                              return (
                                <View key={index2}>
                                  <View style={[styles.rowCenterBetween, { flex: 1, marginTop: 8, marginBottom: 16 }]}>
                                    <View style={[styles.rowCenter, { width: '75%' }]}>
                                      <CheckBox disabled={false} value={item2.isChecked} onChange={() => onChangeValue(item2)}/>
                                      <View style={{ marginLeft: 8 }}>
                                        <Image source={{ uri: item2.banner }} style={styles.image}/>
                                      </View>
                                      <View style={{ marginLeft: 16, width: '45%' }}>
                                        <Text style={[styles.subOrderName, { flexWrap: 'wrap' }]}>
                                          {item2.product}
                                        </Text>
                                        <Text style={styles.subOrderItem}>{item2.qty}{' items'}</Text>
                                      </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginRight: 8, flex: 1, justifyContent: 'flex-end' }}>
                                      <Text style={styles.subOrderName}>{'Rp. '}{thousands(item2.total)}{',-'}</Text>
                                    </View>
                                  </View>
                                </View>
                              );
                            })
                          }
                        </View>
                      </View>
                    </RadioButton.Group>
                  );
                })
              }

            </ScrollView>
            : <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" color="black" />
              {
                isEmptyCart &&
              <Text medium style={{ fontSize: 16, letterSpacing: -0.01, fontStyle: 'italic' }}>{'Have you added some products to cart?'}</Text>
              }

            </View>
        }
      </View>

      <View style={[styles.total, { justifyContent: 'space-between', height: 70 }]}>
        <View style={{ width: (isSingleMerchant) ? '60%' : '50%' }}>
          <Text medium style={{ fontSize: (isSingleMerchant) ? 16 : 18, letterSpacing: -0.01 }}>
            {
              (selectedProducts[0].product === '' ) ?
                <Text medium style={{ fontSize: 18, letterSpacing: -0.01, fontStyle: 'italic' }}>{' no items selected...'}</Text>
                : (isSingleMerchant) ? 'Total' : 'Please checkout within one merchant'
            }
          </Text>
          {
            (isSingleMerchant) ?
              <Text style={{ fontSize: (isSingleMerchant) ? 24 : 16, letterSpacing: -0.01 }}>
                {
                  (selectedProducts[0].product === '' ) ? <ActivityIndicator size="small" color="black" />
                    : (isSingleMerchant) ? `Rp. ${thousands(subTotalPrice)}, -` : ''
                }
              </Text>
              : null
          }
        </View>
        <View>
          <CustomButton text="Checkout"
            color={(selectedProducts[0].product === '' ) ? '#8D92A3' : (isSingleMerchant) ? '#FFA451' : '#8D92A3'}
            isDisabled={(selectedProducts[0].product === '') ? true : (isSingleMerchant) ? false : true}
            width={163} height={45}
            onPress={handleCheckout}
          />
        </View>
      </View>
    </View>
  );
};

export default MyBasketPage;
