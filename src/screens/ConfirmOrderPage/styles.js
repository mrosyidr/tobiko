import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    marginTop: 24,
    flex: 1.5
  },
  address: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    marginTop: 24,
    flex: 1
  },
  image: { height: 80, width: 80, resizeMode: 'cover', borderRadius: 16 },
  divider: {
    backgroundColor: '#F3F1F1',
    height: 5,
    width: Dimensions.get('window').width * 0.9,
    alignSelf: 'center',
    marginTop: 16,
    marginBottom: 8
  },
  shippingPicker: { height: 30, width: 100, alignItems: 'center', justifyContent: 'center' },
  editAddressTextInput: { width: 170, height: 40, backgroundColor: '#F3F1F1' },
  rowCenter: { flexDirection: 'row', alignItems: 'center' },
  rowCenterBetween: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' },
  section: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#020202', fontWeight: 'bold' },
  subSection: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  orderItem: { fontSize: 16, fontFamily: 'Poppins-Medium', color: '#020202' },
  subOrderItem: { fontSize: 13, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  subTotalPrice: { fontSize: 16, fontFamily: 'Poppins-Light', color: '#1ABC9C', fontWeight: 'bold' },
  buttonSection: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#D9435E', fontStyle: 'italic' },
  addressModal: {
    left: '5%',
    top: Dimensions.get('window').height * 0.2,
    height: 350,
    width: '90%',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2
    },
    flexDirection: 'column',
    borderRadius: 30,
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5
  },
  addressModalContainer: {
    marginTop: 10,
    marginRight: 15,
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  },
  paymentModal: {
    left: '5%',
    top: '35%',
    height: '37%',
    width: '90%',
    backgroundColor: 'white',
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2 },
    flexDirection: 'column',
    borderRadius: 30,
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5
  },
  paymentModalContainer: {
    marginTop: 10,
    marginRight: 15,
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  }
});

export default styles;
