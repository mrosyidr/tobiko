import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  ImgBackground: {
    width: Dimensions.get('window').width,
    height: 350,
    position: 'absolute',
    top: 0
  },
  backButton: {
    backgroundColor: '#FFFFFF',
    width: 100, height: 40,
    justifyContent: 'center',
    borderRadius: 100,
    marginTop: 32, marginLeft: 24
  },
  h1: {
    flex: 1,
    marginTop: 175,
    height: Dimensions.get('window').height
  },
  h2: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 16
  },
  h3: {
    marginTop: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  productNameText: {
    color: '#020202',
    fontSize: 20,
    lineHeight: 24,
    fontWeight: 'bold'
  },
  addProductContainer: {
    flexDirection: 'row' ,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addProductButton: {
    height: 35,
    width: 35,
    borderRadius: 8,
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    borderWidth: 1
  },
  addProductText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#020202',
    textAlign: 'center'
  },
  textQuantity: {
    fontSize: 24,
    color: '#020202',
    marginRight: 10,
    marginLeft: 10
  },
  bottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  descriptionContainer: {
    backgroundColor: '#FFFFFF',
    marginTop: 350,
    paddingHorizontal: 16,
    height: Dimensions.get('window').height * 0.6,
    justifyContent: 'space-between'
  },
  textDescription: {
    color: '#8D92A3',
    fontSize: 16,
    lineHeight: 24,
    marginTop: 16
  },
  textFeature: {
    color: '#8D92A3',
    fontSize: 16,
    marginTop: 16,
    lineHeight: 24
  },
  subText: {
    color: '#020202',
    fontSize: 16,
    marginTop: 24,
    lineHeight: 24,
    fontWeight: 'bold'
  },
  textPrice: {
    color: '#020202',
    fontSize: 18,
    marginTop: 4
  },
  rating: {
    flexDirection: 'row',
    marginTop: 6,
    alignItems: 'center'
  },
  backText: {
    fontSize: 16,
    fontWeight: '500'
  }
});

export default styles;
