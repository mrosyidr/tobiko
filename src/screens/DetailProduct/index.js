import React, { useState, useEffect } from 'react';
import { Alert, View, Text, TouchableOpacity, ImageBackground, ScrollView, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

import { CustomButton } from '../../components/';
import { getToken, showMessage } from '../../utils/';
import postProductToCart from '../../api/postProductToCart/index';

import styles from './styles';

function DetailProduct({ route, navigation }){
  const { id } = route.params;
  const [selectedItem] = useState(id[0]);
  const [count, setCount] = useState(0);
  const [token, setToken] = useState('');
  //   console.log('ini dari sebelah : ', selectedItem);

  useEffect(() => {
    processToken();
  },[]);

  const processToken = async () => {
    try {
      const mytoken = await getToken();
      if (mytoken){
        setToken(mytoken);
      }
    } catch (e) {
      showMessage('Please Login', 'warning');
    }
  };

  const handleAddItem = ( name, merchant, idx, quantity) =>{
    if (token === ''){
      showMessage('Please Login', 'warning');
      navigation.replace('SignIn');
    } else {
      if (quantity > 0){
        handleAddToCart(name, merchant, idx, quantity);
      } else {
        handleNoItem();
      }
    }
  };

  const handleAddToCart = (name, merchant, idx, quantity) => {
    Alert.alert(
      'Add to Basket',
      `Add ${quantity} ${name} to Cart?`, [{
        text: 'Cancel',
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => {
          addToCart(token, merchant, idx, quantity);
        }
      }], {
        cancelable: false
      }
    );
    return true;
  };

  const handleNoItem = () => {
    Alert.alert(
      'Add to Basket',
      'No Item Added, back to Home?', [{
        text: 'Cancel',
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => navigation.navigate('HomePage')
      }], {
        cancelable: false
      }
    );
    return true;
  };

  const addToCart = async (auth, merchant, idx, quantity) => {
    try {
      const res = await postProductToCart(auth,merchant,idx,quantity);
      if (res.data.code === 200){
        showMessage(res.data.message, 'success');
        navigation.navigate('HomePage');
      } else {
        showMessage(res.data.message, 'warning');
        navigation.replace('SignIn');
      }
    } catch (e) {
      throw e;
    }
  };

  // console.log('ini token detail : ', token)
  return (
    <View>
      <ImageBackground style={styles.ImgBackground}
        source={{ uri: selectedItem.banner }}>
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.navigate('HomePage')}>
          <View style={{ flexDirection: 'row' }}>
            <Icons name="chevron-back-outline" size={25} color={'#000000'}/>
            <Text style={styles.backText}>
                Go Back
            </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.h1}>
          <ScrollView style={styles.h2}>
            <View style={styles.h3}>
              <View style={{ width: Dimensions.get('window').width * 0.6 }}>
                <Text style={styles.productNameText}>
                  {selectedItem.name}
                </Text>
                <View style={styles.rating}>
                  <Icons name="star" size={16} color={selectedItem.averageRating >= 2 ? '#FFC700' : '#ECECEC'}/>
                  <Icons name="star" size={16} color={selectedItem.averageRating >= 4 ? '#FFC700' : '#ECECEC'}/>
                  <Icons name="star" size={16} color={selectedItem.averageRating >= 6 ? '#FFC700' : '#ECECEC'}/>
                  <Icons name="star" size={16} color={selectedItem.averageRating >= 8 ? '#FFC700' : '#ECECEC'}/>
                  <Icons name="star" size={16} color={selectedItem.averageRating >= 10 ? '#FFC700' : '#ECECEC'}/>
                  <Text style={{ color: '#8D92A3', marginLeft: 8 }}>
                    {selectedItem.averageRating}   |   Rp {selectedItem.price}
                  </Text>
                </View>
              </View>
              <View style={styles.addProductContainer}>
                <TouchableOpacity
                  style={styles.addProductButton}
                  onPress={()=> {
                    if (selectedItem.quantity > 0){
                      selectedItem.quantity = selectedItem.quantity - 1;
                      setCount(count - 1);
                    }}
                  }>
                  <Text style={styles.addProductText}>
                    -
                  </Text>
                </TouchableOpacity>
                <Text style={styles.textQuantity}>
                  {selectedItem.quantity}
                </Text>
                <TouchableOpacity
                  style={styles.addProductButton}
                  onPress={()=> {
                    selectedItem.quantity = selectedItem.quantity + 1;
                    setCount(count + 1);
                  }}>
                  <Text style={styles.addProductText}>
                    +
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </ImageBackground>
      <View style={styles.descriptionContainer}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
            <Text style={styles.textDescription}>
              {selectedItem.description}
            </Text>
          </View>
          <View>
            <Text style={styles.subText}>
                Feature :
            </Text>
            <Text style={styles.textFeature}>
              {selectedItem.feature}
            </Text>
          </View>
        </ScrollView>
        <View style={styles.bottomContainer}>
          <View>
            <Text style={{ color: '#8D92A3' }}>
                Total Price :
            </Text>
            <Text style={styles.textPrice}>
                Rp {selectedItem.price * selectedItem.quantity}
            </Text>
          </View>
          <CustomButton text={'Add to Cart'} width={225}
            height={48}
            onPress={() => {
              handleAddItem( selectedItem.name, selectedItem.merchants[0].id, selectedItem.id, selectedItem.quantity);
            }}/>
        </View>
      </View>
    </View>
  );
}

export default DetailProduct;
