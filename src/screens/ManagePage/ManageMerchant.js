/* eslint-disable no-console */
import React, { useEffect } from 'react';
import { View, SafeAreaView, ScrollView,Image } from 'react-native';
import { Text, Title, Subheading, Button, Snackbar } from 'react-native-paper';
import { TabBar, TabView, SceneMap } from 'react-native-tab-view';
import AsyncStorage from '@react-native-async-storage/async-storage';

import activateMerchant from '../../api/putActivateMerchant';
import deactivateMerchant from '../../api/putDeactivateMerchant';
import deleteMerchant from '../../api/putDeleteMerchant';
import getMerchantDetail from '../../api/getMerchantDetail';


const ManageMerchant = (props) => {

  let { navigation,route } = props;
  let { params } = route;
  let idmerchant = params.id;
  const initialLayout = { width: 400 };

  const [index,setIndex] = React.useState(0);
  const [listData,setListData] = React.useState([]);
  const [routes] = React.useState([
    { key: 'settings', title: 'Settings' },
    { key: 'products', title: 'Products' }
  ]);

  const [visible, setVisible] = React.useState(false);
  const ShowSnackbar = () => setVisible(!visible);
  const DismissSnackbar = () => setVisible(false);


  useEffect(()=>{
    GetDetailMerchant();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const HandleActivateMerchant = async() => {
    let token = await AsyncStorage.getItem('@token');
    activateMerchant(token,idmerchant).then((result)=>{
      console.log(result);
      ShowSnackbar();
    }).catch((err)=>{
      console.log(err);
    });
  };

  const HandleDeactivateMerchant = async() => {
    let token = await AsyncStorage.getItem('@token');
    deactivateMerchant(token,idmerchant).then((result)=>{
      console.log(result);
      ShowSnackbar();
    }).catch((err)=>{
      console.log(err);
    });
  };

  const HandleDeleteMerchant = async() => {
    let token = await AsyncStorage.getItem('@token');
    deleteMerchant(token,idmerchant).then((result)=>{
      console.log(result);
      ShowSnackbar();
    }).catch((err)=>{
      console.log(err);
    });
  };

  const GetDetailMerchant = async() => {
    let token = await AsyncStorage.getItem('@token');
    getMerchantDetail(token,idmerchant).then((result)=>{
      setListData(result.data.data[0].products.map(function(item){
        return ({
          name: item.name,
          price: item.price,
          banner: item.banner
        });
      }));
    }).catch((err)=>{
      console.log(err);
    });
  };


  const SettingsRoute = () => {
    return (
      <View style={{ flex: 1,flexDirection: 'column',alignItems: 'flex-start' }}>
        <View style={{ marginTop: 15, flex: 2, flexDirection: 'row' }}>

          <Image source={{ uri: params.photo }} style={{ marginHorizontal: 3,width: 78,height: 78,flex: 2 }}/>
          <View style={{ marginVertical: 8, marginHorizontal: 20,flex: 5, flexDirection: 'column' }}>
            <Text style={{ fontSize: 18,color: '#222222',marginVertical: 2 }}>{params.name}</Text>
            <Text style={{ fontSize: 16,color: '#222222',marginVertical: 2 }}>{params.verificationStatus}</Text>
          </View>

        </View>
        <Button color="#555555" style={{ flex: 1,fontSize: 23 }} mode="text" onPress={()=>HandleActivateMerchant()} >Activate Merchant</Button>
        <Button color="#555555" style={{ flex: 1,fontSize: 23 }} mode="text" onPress={()=>HandleDeactivateMerchant()} >Deactivate Merchant</Button>
        <Button color="#555555" style={{ flex: 1,fontSize: 23 }} mode="text" onPress={()=>HandleDeleteMerchant()}>Delete Merchant</Button>

        <View style={{ flex: 7 }} />
      </View>
    );
  };


  const ProductsRoute = () => {
    return (
      <ScrollView>
        <View style={{ flex: 1,flexDirection: 'column' }}>
          {listData.map((item,key)=>(
            <View key={key} style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

              <Image source={{ uri: item.banner }} style={{ width: 78,height: 78,flex: 3 }}/>
              <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
                <Text style={{ fontSize: 17,color: '#111111',marginVertical: 2 }}>{item.name}</Text>
                <Text style={{ fontSize: 15,color: '#444444',marginVertical: 2 }}>{item.price}</Text>
              </View>
              {/* <Button style={{marginVertical:10,color:'black',backgroundColor:'#ffa500',flex:2,widht:70,height:40}} mode="contained">Remove</Button>   */}
            </View>
          ))}
        </View>
        <View style={{ flex: 5 }} />
      </ScrollView>


    );
  };

  const renderTabBar = propstab => {
    return (
      <TabBar
        {...propstab}
        indicatorStyle={{ backgroundColor: 'orange' }}
        style={{ backgroundColor: 'white' }}
        renderLabel={({ route, focused, color }) => (
          <Text style={{ color: '#666666', fontSize: 15 }}>
            {route.title}
          </Text>
        )}
      />
    );
  };

  const renderScene = SceneMap({
    settings: SettingsRoute,
    products: ProductsRoute
  });


  return (
    <SafeAreaView style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 5 }}>
      <View style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 10 }}>
        <Title style={{ fontSize: 30 }}>Administration</Title>
        <Subheading style={{ marginBottom: 20 }}>Manage Merchant</Subheading>
        <TabView renderTabBar={renderTabBar}
          navigationState={{ index, routes }}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={initialLayout}
          style={{ fontSize: 40 }}/>
        <Snackbar
          visible={visible}
          onDismiss={DismissSnackbar}>
                                        Success!
        </Snackbar>
      </View>
    </SafeAreaView>
  );

};

export default ManageMerchant;

