import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  h1: {
    fontSize: 22,
    marginBottom: 5
  },
  h2: {
    fontSize: 20,
    marginBottom: 5
  },
  h3: {
    fontSize: 17,
    marginBottom: 5
  },
  h4: {
    fontSize: 14,
    marginBottom: 5
  },
  modal: {
    left: '15%',
    top: '20%',
    height: '25%',
    width: '70%',
    backgroundColor: 'white',
    // borderColor: 'grey',
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2 },
    flexDirection: 'column',
    borderRadius: 30,
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5
    // borderWidth: 0.3,
  },
  modal2: {
    marginTop: 10,
    marginRight: 15,
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  },
  totalloan: {
    backgroundColor: '#f2f2f2',
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '1%'
  },
  container2: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 85
  },
  container1: {
    flexDirection: 'row',
    margin: 10,
    marginHorizontal: '4%'
  },
  historyContainer: {
    flex: 0.1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginTop: 10 }
});

export default styles;
