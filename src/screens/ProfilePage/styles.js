import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    h1:{
        fontSize: 22,
        marginBottom : 5
    },
    h2:{
        fontSize: 20,
        marginBottom : 5
    },
    h3:{
        fontSize: 17,
        marginBottom : 5
    },
    h4:{
        fontSize: 14,
        marginBottom : 5
    },
    label: (textcolor) => ({
        color: textcolor,
        textAlign: 'center',
        fontSize:14,
        fontFamily: 'Poppins-Light'
    }),
    modal:{
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    modal2:{
        marginTop: 10,
        marginRight: 15,
        alignItems:'flex-end',
        justifyContent:'flex-start',
    },
    container:{
        flexDirection:'column', 
        paddingHorizontal:30, 
        alignItems:'center', 
        justifyContent:'space-around', 
        height:120
    },
    container2:{
        flexDirection:'column', 
        paddingHorizontal:10, 
        alignItems:'center'}
})

export default styles