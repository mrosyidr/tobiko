import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  subtitleBar: {
    height: Dimensions.get('window').height * 0.05,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    justifyContent: 'center' ,marginTop: 16
  },
  container: {
    backgroundColor: '#FFFFFF',
    height: Dimensions.get('window').height,
    flex: 1
  }
});

export default styles;
