import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import { Logo } from '../../assets';
import { Gap } from '../../components';
import { getToken } from '../../utils';

const SplashScreen = ({ navigation }) => {
  const processToken = async () => {
    const token = await getToken();
    const isAdminToken = await AsyncStorage.getItem('@isAdmin');
    const isMerchantToken = await AsyncStorage.getItem('@isMerchant');

    if (token) {
      if (isAdminToken === 'true' || isAdminToken === 'True') {
        setTimeout(() => {
          navigation.replace('AdminPage');
        }, 2000);
      } else {
        if (isMerchantToken === 'true' || isMerchantToken === 'True') {
          setTimeout(() => {
            navigation.replace('MerchantHome');
          }, 2000);
        } else {
          setTimeout(() => {
            navigation.replace('HomePage');
          }, 2000);
        }
      }
    }
    else {
      setTimeout(() => {
        navigation.replace('HomePage');
      }, 2000);
    }
  };

  useEffect(() => {
    processToken();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.container}>
      <Logo />
      <Gap height={38} />
      <Text style={styles.text}>Tobiko</Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFC700',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: { fontSize: 32, color: '#020202', fontFamily: 'Poppins-Medium' }
});
