import React, { useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import { launchImageLibrary } from 'react-native-image-picker';
import { useDispatch } from 'react-redux';

import { Logo } from '../../assets';
import { CustomButton, Gap, Header, CustomTextInput } from '../../components';
import { useForm } from '../../utils';


const SignUpMerchant = ({ navigation }) => {
  const [fileUri, SetFileuri] = useState('');
  const [form, setForm] = useForm({
    name: '',
    email: '',
    password: ''
  });
  //const [photo, setPhoto] = useState(profpic);
  const dispatch = useDispatch();


  const onSubmit = () => {
    dispatch({ type: 'SET_REGISTER', value: form });
    navigation.navigate('SignUpMerchantAddress');
    //console.log('data', form);
  };
  //--------------IMAGE PICKER-----------------
  const addPhoto = () => {
    let options = {
      title: 'Select Avatar',
      cameraType: 'front',
      mediaType: 'photo',
      maxWidth: 300,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
      includeBase64: true
    };
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        return;
      }
      SetFileuri(response.uri);
      //console.log('response:',response);
      //sendAvatar(response);
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <View style={styles.page}>
        <Header
          title="Sign Up Merchant"
          subTitle="Register merchant"
          onBack={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <View style={styles.photo}>
            <TouchableOpacity>
              <View style={styles.borderPhoto}>
                <Logo/>
                {/* <Image
                  source={fileUri ? { uri: fileUri } : // if clicked a new img
                    require('../../assets/Dummy/profile-dummy.png')} //else show random
                  style={styles.photoContainer}
                /> */}
              </View>
            </TouchableOpacity>
          </View>
          <CustomTextInput
            label="Full Name"
            placeholder="Type your full name"
            value={form.name}
            onChangeText={(value) => setForm('name', value)}
          />
          <Gap height={16} />
          <CustomTextInput
            label="Email Address"
            placeholder="Type your email address"
            value={form.email}
            keyboardType="email-address"
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={16} />
          <CustomTextInput
            label="Password"
            placeholder="Type your password"
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />
          <Gap height={24} />
          <CustomButton text="Continue" onPress={onSubmit} />
        </View>
      </View>
    </ScrollView>
  );
};

export default SignUpMerchant;

const styles = StyleSheet.create({
  scroll: { flexGrow: 1 },
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    marginTop: 24,
    flex: 1
  },
  photo: { alignItems: 'center', marginTop: 26, marginBottom: 16 },
  borderPhoto: {
    borderWidth: 1,
    borderColor: '#8D92A3',
    width: 110,
    height: 110,
    borderRadius: 110,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoContainer: {
    width: 90,
    height: 90,
    borderRadius: 90,
    backgroundColor: '#F0F0F0',
    justifyContent: 'center',
    alignItems: 'center'
  },
  addPhoto: {
    fontSize: 14,
    fontFamily: 'Poppins-Light',
    color: '#8D92A3',
    textAlign: 'center'
  }
});
