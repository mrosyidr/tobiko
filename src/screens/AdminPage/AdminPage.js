import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useRef } from 'react';
import { FlatList, View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/Ionicons';

import { CustomButton } from '../../components';
import { showMessage } from '../../utils';

function AdminPage(props) {
  const { navigation } = props;
  const flatListRef = useRef();
  const [refreshing] = useState(true);
  const [selectedId, setSelectedId] = useState(1);
  const [] = useState([]);
  const [adminTab] = useState([
    {
      id: 1,
      name: 'Merchant'
    },
    {
      id: 2,
      name: 'Loan'
    },
    {
      id: 3,
      name: 'Loan Payment Verification'
    }
  ]);
  const [merchantList] = useState([
    {
      id: 1,
      name: 'Merchant A',
      totalItem: 3,
      totalPrice: 18000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 2,
      name: 'Merchant B',
      totalItem: 4,
      totalPrice: 24000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 3,
      name: 'Merchant C',
      totalItem: 4,
      totalPrice: 24000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    }
  ]);

  const [loanList] = useState([
    {
      id: 1,
      name: 'Customer 1',
      totalLoan: 18000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 2,
      name: 'Customer 2',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 3,
      name: 'Customer 3',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 4,
      name: 'Customer 4',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 5,
      name: 'Customer 5',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    }
  ]);

  const [paymentLoanList] = useState([
    {
      id: 1,
      name: 'Payment Loan 1',
      totalLoan: 18000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 2,
      name: 'Payment Loan 2',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 3,
      name: 'Payment Loan 3',
      totalLoan: 21000000,
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    }
  ]);


  const toTop = () => {
    flatListRef.current.scrollToOffset({ animated: true, offset: 0 });
  };

  const Item = ({ item, onPress, color, div, style, ...others }) => (
    <TouchableOpacity
      onPress={onPress}
      style={[
        { height: 50, backgroundColor: '#FFFFFF', marginRight: 24 },
        style
      ]}>
      <View
        style={{
          flex: 1,
          alignContent: 'center',
          justifyContent: 'center'
        }}>
        <Text
          style={[
            {
              lineHeight: 32,
              fontWeight: '500',
              letterSpacing: -0.02,
              fontSize: 16,
              color: '#020202'
            },
            style
          ]}
          {...others}>
          {item.name}
        </Text>
        {/* <Divider style={{ backgroundColor: '#FFA451', height : {div}, width : 30}} /> */}

      </View>
    </TouchableOpacity>
  );

  const renderButton = ({ item }) => {
    const color = item.id === selectedId ? '#27214D' : '#938DB5';
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          //   handleGetCategory(item.id)
          toTop();
        }}
        style={{ color }}
      />
    );
  };


  const renderItemMerchant = ({ item }) => {
    return (
      <View style={{ flex: 1, width: '100%',
        height: 80, backgroundColor: '#FFFFFF', alignContent: 'center', justifyContent: 'center', marginTop: 3, marginBottom: 3 }} onPress={() => {
        // eslint-disable-next-line no-console
        console.log('yang dibuka dari onPress: ', item.id);
        // moveScreen({id : item.id})
      }}>
        <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Image
            source={{ uri: item.avatar }}
            style={{ height: 65, width: 65, borderRadius: 8 }}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '83%', marginLeft: 5 }}>
            <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '400', color: '#020202' }}>
                {item.name}
              </Text>
              <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                {item.totalItem} items • IDR {item.totalPrice}
              </Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <CustomButton text={'Manage'} width={110} height={48} />
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderItemLoan = ({ item }) => {
    return (
      <View style={{ flex: 1, width: '100%', height: 80,
        backgroundColor: '#FFFFFF', alignContent: 'center', justifyContent: 'center', marginTop: 3, marginBottom: 3 }} onPress={() => {
        // eslint-disable-next-line no-console
        console.log('yang dibuka dari onPress: ', item.id);
        // moveScreen({id : item.id})
      }}>
        <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Image
            source={{ uri: item.avatar }}
            style={{ height: 65, width: 65, borderRadius: 8 }}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '83%', marginLeft: 5 }}>
            <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '400', color: '#020202' }}>
                {item.name}
              </Text>
              <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                                IDR {item.totalLoan}
              </Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <CustomButton text={'Manage'} width={110} height={48} />
            </View>
          </View>
        </View>
      </View>
    );
  };

  const renderItemPaymentLoan = ({ item }) => {
    return (
      <View style={{ flex: 1, width: '100%', height: 80,
        backgroundColor: '#FFFFFF', alignContent: 'center', justifyContent: 'center', marginTop: 3, marginBottom: 3 }} onPress={() => {
        //console.log('yang dibuka dari onPress: ', item.id)
        // moveScreen({id : item.id})
      }}>
        <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
          <Image
            source={{ uri: item.avatar }}
            style={{ height: 65, width: 65, borderRadius: 8 }}
          />
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '83%', marginLeft: 5 }}>
            <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '400', color: '#020202' }}>
                {item.name}
              </Text>
              <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                                IDR {item.totalLoan}
              </Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <CustomButton text={'Manage'} width={110} height={48} onPress={() => navigation.navigate('AdminPaymentLoan', { id: item.id })} />
            </View>
          </View>
        </View>
      </View>
    );
  };


  const handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('@token');
      await AsyncStorage.removeItem('@isAdmin');
      navigation.replace('SignIn');
    } catch (error) {
      showMessage('Error Logout', error.response);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: '15%', backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ flex: 1 }}>
          <Text style={{ marginLeft: 20, fontSize: 22, fontFamily: 'Poppins-Medium', color: '#020202', fontWeight: 'bold' }}>Administration</Text>
          <Text style={{ marginLeft: 20, fontSize: 14, fontFamily: 'Poppins-Light', color: '#8D92A3' }}>You are Admin</Text>
        </View>
        <View style={{ marginRight: 20 }}>
          <CustomButton text="Logout" width={110} height={48} onPress={() => handleLogout()} />
        </View>
      </View>

      <View style={{ height: '10%', backgroundColor: '#FFFFFF', paddingHorizontal: 24, justifyContent: 'center', marginTop: 24 }}>
        <View style={{ flexDirection: 'row', backgroundColor: '#F3F1F1', borderRadius: 8 }}>
          <Icons name="search" size={20} color="#979797" style={{ marginLeft: 13, marginTop: 17 }} />
          <TextInput
            placeholder="Search"
            placeholderTextColor="#8E8E93"
            style={{
              height: 45,
              width: 200,
              fontSize: 14,
              lineHeight: 21,
              paddingLeft: 13
            }}
          />
        </View>
      </View>
      <View style={{ width: '100%', backgroundColor: '#FFFFFF', paddingHorizontal: 24, height: '6%', marginTop: 10 }}>
        <View style={{ height: 50, flex: 1, alignContent: 'center' }}>
          <FlatList
            data={adminTab}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            renderItem={renderButton}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshing}
            extraData={selectedId}
            // onRefresh={getData}
          />
        </View>
      </View>
      <Divider style={{ backgroundColor: '#F2F2F2', height: 1, width: '100%' }} />
      <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
        <View style={{ flex: 1 }}>
          <FlatList
            ref={flatListRef}
            data={selectedId === 1 ? merchantList : selectedId === 2 ? loanList : paymentLoanList}
            showsVerticalScrollIndicator={false}
            // showsHorizontalScrollIndicator={false}
            // horizontal = {true}
            renderItem={selectedId === 1 ? renderItemMerchant : selectedId === 2 ? renderItemLoan : renderItemPaymentLoan}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshing}
            // extraData={selectedId}
            // onRefresh={getData}
          />
        </View>
      </View>
    </View>
  );
}

export default AdminPage;
