/* eslint-disable no-console */
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { TextInput } from 'react-native';
import { ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Config from 'react-native-config';
import { StyleSheet, Text, View, Image } from 'react-native';

import { CustomButton, Gap, Header } from '../../components';
import { getOrderDetails } from '../../redux/actions';
import { getToken } from '../../utils';

const MerchantInputResi = ({ route, navigation }) => {

  const { id } = route.params;
  const [resi, setResi] = useState('');
  const dispatch = useDispatch();
  const { orderDetail } = useSelector((state) => state.orderReducer);

  useEffect(() => {
    console.log('useEffect detail');
    //handlerOrderDetail('a5ce36c8-d2d9-45f6-8411-d9509aa7a24d');
    dispatch(getOrderDetails(id));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleInputResi = (id) => {
    getToken().then((res) => {
      //console.log('res value:', res)
      console.log('id', id);
      console.log('resi', resi);
      axios(Config.BASE_URL + '/api/merchant/shipping/' + id, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY,
          'Authorization': 'Bearer ' + res
        },
        data: { 'shippingTrackingNumber': resi }
      }).then((response) => {
        console.log(response.data);
        navigation.replace('MerchantHome');
      }).catch((err) => {
        console.log('Errornya : ', err);
        //setLoading(false);
        //dispatch(setLoading(false))
      });
    });

  };

  return (
    <View style={styles.content}>
      <Header title="Input Resi" subTitle="merchant order details" onBack={() => navigation.goBack()} />
      <Gap height={12} />
      <View style={{ alignItems: 'center' }}>
        <TextInput
          style={{
            borderRadius: 20, borderWidth: 1, borderColor: '#333', width: 300, textAlign: 'center' }} placeholder="Please Input Resi"
          onChangeText={(value) => setResi(value)}></TextInput>
        <Gap height={12} />
        <CustomButton text="Input Resi" width={300} onPress={() => handleInputResi(id)} />
      </View>
      <Gap height={12} />
      <ScrollView>
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.titleOrder}>Item Ordered</Text>
          {orderDetail.items ? (orderDetail.items.map(item => {
            return (
              <View style={styles.container} key={item.id}>
                <Image source={{ uri: item.product.banner }} style={styles.image} />
                <View style={styles.content}>
                  <Text style={styles.title}>{item.product.name} </Text>
                  <Text style={styles.price}>IDR {item.price}</Text>
                </View>
                <Text style={styles.items}>{item.quantity} items</Text>
              </View>
            );
          })) : (<Text>Data Kosong</Text>)}
        </View>
      </ScrollView>
    </View>
  );
};

export default MerchantInputResi;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: 8,
    alignItems: 'center',
    marginLeft: 20
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 8,
    overflow: 'hidden',
    marginRight: 12
  },
  content: { flex: 1 },
  title: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#020202'
  },
  titleOrder: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#020202',
    marginLeft: 16
  },
  price: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: '#8D92A3'
  },
  items: { fontSize: 13, fontFamily: 'Poppins-Regular', color: '#8D92A3', marginRight: 16 },
  date: { fontSize: 10, fontFamily: 'Poppins-Regular', color: '#8D92A3' },
  status: (status) => ({
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    color: status === 'CANCELLED' ? '#D9435E' : '#1ABC9C'
  }),
  row: { flexDirection: 'row', alignSelf: 'center', marginBottom: 16 },
  dot: {
    width: 3,
    height: 3,
    borderRadius: 3,
    backgroundColor: '#8D92A3',
    marginHorizontal: 4
  }
});
