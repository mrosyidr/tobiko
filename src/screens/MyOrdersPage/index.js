import * as React from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import { Divider } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Header, HorizontalButton, MyOrderCard } from '../../components';
import { showMessage } from '../../utils/';
import getAllOrder from '../../api/getAllOrder/index';



export default function MyOrdersPage(props) {
  const { navigation } = props;
  const [selectedId, setSelectedId] = React.useState(1);
  const [allOrder, setAllOrder] = React.useState([]);
  const [listPerStatus, setListPerStatus] = React.useState([]);
  const [routes] = React.useState([
    { id: 1, name: 'Waiting for Payment' },
    { id: 2, name: 'Payment Received' },
    { id: 3, name: 'On Delivery' },
    { id: 4, name: 'Order Finished' }
  ]);

  React.useEffect(() => {
    processToken();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  React.useEffect(() => {
    processToken();
    return () => {
      // eslint-disable-next-line no-console
      console.log('-- MyOrdersPage components cleanup and unmounted');
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const processToken = async () => {
    try {
      const mytoken = await AsyncStorage.getItem('@token');
      if (mytoken){
        getMyOrder(mytoken);
      }
    } catch (e) {
      showMessage('Please Login', 'warning');
    }
  };

  const getMyOrder = async(auth) => {
    try {
      const res = await getAllOrder(auth);
      setAllOrder(res.data.data);
      setListPerStatus(res.data.data.filter(obj => {return obj.workflow.id === 1;}));
    } catch (e) {
      showMessage('Please Login', 'warning');
    }
  };

  const handleGetCategory = (id) => {
    switch (id){
      case 1 : {
        return (
          setListPerStatus(allOrder.filter(obj => {return obj.workflow.id === 1 || obj.workflow.id === 8;}))
        );
      }
      case 2 : {
        return (
          setListPerStatus(allOrder.filter(obj => {return (obj.workflow.id === 2 || obj.workflow.id === 3);}))
        );
      }
      case 3 : {
        return (
          setListPerStatus(allOrder.filter(obj => {return obj.workflow.id === 4;}))
        );
      }
      case 4 : {
        return (
          setListPerStatus(allOrder.filter(obj => {return (obj.workflow.id === 5 || obj.workflow.id === 6 || obj.workflow.id === 7);}))
        );
      }
      default : {
        return (
          setListPerStatus([])
        );
      }
    }
  };

  // eslint-disable-next-line no-console
  console.log(listPerStatus);

  const renderButton = ({ item }) => {
    const color = item.id === selectedId ? '#27214D' : '#938DB5';
    return (
      <HorizontalButton
        name={item.name}
        onPress={() => {
          // setListPerCategory([]);
          setSelectedId(item.id);
          handleGetCategory(item.id);
          // toTop();
          // setRefreshing(true);
        }}
        style={{ color }}
      />
    );
  };

  const renderItemOrder = ({ item }) => {
    return (
      <MyOrderCard
        name={item.invoiceNumber}
        status={item.workflow.name}
        banner={item.customer.photo}
        OnPress={() => {
          moveToDetailProduct({ id: item.id });
        }}
      />
    );
  };

  const moveToDetailProduct = ({ id }) => {
    // navigation.navigate('DetailScreen', { id: listPerCategory.filter(obj => {return obj.id === id;}) });
    // alert(id);
    navigation.navigate('MyOrderDetail', { id: id });
  };

  return (
    <View style={styles.page}>
      <Header title="My Orders" subTitle="Wait for the best products"
        onBack={() => {
        // navigation.navigate('ProfilePage');
          navigation.replace('ProfilePage');
        }} />
      <View style={styles.container}>
        <View style={styles.horizontal}>
          <View style={styles.buttons}>
            <FlatList
              data = {routes}
              showsHorizontalScrollIndicator={false}
              horizontal = {true}
              renderItem={renderButton}
              keyExtractor={(item) => item.id.toString()}
              extraData={selectedId}
            />
          </View>
        </View>
        <Divider style={styles.divider} />
        {listPerStatus.length > 0 ?
          <View style={styles.order}>
            <View style={{ flex: 1 }}>
              <FlatList
              // ref={flatListRef}
                data = {listPerStatus}
                showsVerticalScrollIndicator={false}
                renderItem={renderItemOrder}
                keyExtractor={(item) => item.id.toString()}
              // refreshing={refreshing}
              // onRefresh={handleRefresh}
              // onEndReached={handleLoadMore}
              />
            </View>
          </View>
          : <View style={styles.order}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 16, fontFamily: 'Poppins-Medium', color: '#8D92A3', textAlign: 'center' }}>
                No Order with status {selectedId === 1 ? 'Waiting for Payment' : selectedId === 2 ?
                  'Paid or Prepared by Merchant' : selectedId === 3 ? 'Shipped' : 'Finished'}
              </Text>
            </View>
          </View>
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  tabPage: {
    flex: 1
  },
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    marginTop: 24,
    marginBottom: 24,
    flex: 1
  },
  horizontal: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 24,
    height: '6%'
  },
  buttons: {
    height: 50,
    flex: 1,
    alignContent: 'center'
  },
  divider: {
    backgroundColor: '#F2F2F2',
    height: 2,
    width: '100%'
  },
  order: {
    backgroundColor: '#FFFFFF',
    height: '100%',
    flex: 1
  },
  orderItem: { fontSize: 16, fontFamily: 'Poppins-Medium', color: '#020202' },
  subOrderItem: { fontSize: 13, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  subOrderInfo: { fontSize: 10, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  subOrderCancelled: { fontSize: 10, fontFamily: 'Poppins-Light', color: '#D9435E' }
});
