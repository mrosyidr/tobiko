import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  ImgBackground: {
    width: '100%',
    position: 'absolute',
    top: 0
  },
  backButton: {
    backgroundColor: '#FFFFFF',
    width: 150, height: 40,
    justifyContent: 'center',
    borderRadius: 100,
    marginTop: 32, marginLeft: 24
  },
  titleContainer: {
    // flex: 1,
    marginTop: 20,
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 16
  },
  textDescription: {
    color: '#8D92A3',
    fontSize: 14,
    lineHeight: 24,
    marginTop: 16,
    fontFamily: 'Poppins-Regular',
    textAlign: 'justify'
  },
  descriptionContainer: {
    backgroundColor: '#FFFFFF',
    height: Dimensions.get('window').height,
    justifyContent: 'space-between'
  },
  textTitle: {
    color: '#020202',
    fontSize: 20,
    lineHeight: 24,
    fontWeight: 'bold',
    marginTop: 12
  },
  textSubtitle: {
    color: '#020202',
    fontSize: 16,
    lineHeight: 24,
    fontWeight: 'bold',
    marginTop: 12
  },
  textDate: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    marginTop: 16,
    lineHeight: 16,
    color: 'red'
  },
  h1: {
    backgroundColor: '#FFFFFF',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 16
  },
  h2: {
    marginTop: 24,
    alignContent: 'center'
  },
  backText: {
    fontSize: 16,
    fontWeight: '500'
  }
});

export default styles;
