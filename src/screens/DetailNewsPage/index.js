import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';
import { Divider } from 'react-native-elements';

import { Gap } from '../../components';

import styles from './styles';


function DetailNewsPage({ route, navigation }){
  const { id } = route.params;
  const [selectedItem] = useState(id[0]);

  return (
  // <ScrollView contentContainerStyle={{flex:1}}>
    <View style={{ backgroundColor: '#FFA451', flex: 1 }}>
      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.replace('HomePage')}>
        <View style={{ flexDirection: 'row' }}>
          <Icons name="chevron-back-outline" size={25} color={'#000000'}/>
          <Text style={styles.backText}>
                Back to Home
          </Text>
        </View>
      </TouchableOpacity>
      <View style={styles.titleContainer}>
        <Image
          source={{ uri: selectedItem.thumbnail }}
          style={{ height: 200, width: '100%', borderRadius: 8, marginTop: 16 }}
        />
        <View style={{ flexDirection: 'row', marginTop: 16, alignItems: 'center', justifyContent: 'space-between' }}>
          <Text style={{ fontFamily: 'Poppins-Light' }}>
                      Share
          </Text>
          <Icons name="share-social" size={25}/>
        </View>
        <Divider style={{ backgroundColor: '#F2F2F2', height: 2, width: '100%', marginTop: 8 }}/>
      </View>
      <ScrollView style={{ paddingHorizontal: 16, backgroundColor: '#FFFFFF' }}>
        <Text style={styles.textTitle}>
          {selectedItem.title}
        </Text>
        <Text style={styles.textDescription}>
          {selectedItem.subtitle}
        </Text>
        <Gap height={24}/>
        <Text style={styles.textSubtitle}>
          Description
        </Text>
        <Text style={styles.textDescription}>
          {selectedItem.description}
        </Text>
        <Gap height={24}/>
        <Text style={styles.textSubtitle}>
          Terms and Condition
        </Text>
        <Text style={styles.textDescription}>
          {selectedItem.tnc}
        </Text>
        <Gap height={24}/>
      </ScrollView>
    </View>
  // </ScrollView>
  );
}

export default DetailNewsPage;
