/* eslint-disable no-console */
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { RefreshControl, View, SafeAreaView, ScrollView,Image } from 'react-native';
import { Text, Title, Subheading } from 'react-native-paper';
import { TabBar, TabView, SceneMap } from 'react-native-tab-view';

import { CustomButton } from '../../components';
import getMerchant from '../../api/getMerchant';
import getLoan from '../../api/getLoanMerchant';
import getPaymentApproval from '../../api/getPaymentApproval';
import getLoanPayment from '../../api/getLoanPayment';

const MerchantMain = (props) => {

  let { navigation } = props;
  const [listMerchant,setListMerchant] = useState([]);
  const [listLoan, setListLoan] = useState([]);
  const [listPayment, setListPayment] = useState([]);
  const [listLoanPayment, setListLoanPayment] = useState([]);
  const initialLayout = { width: 400 };
  const [index,setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'merchant', title: 'Merchant' },
    { key: 'loan', title: 'Loan' },
    { key: 'paymentapproval', title: 'Payment Approval' },
    { key: 'loanpayment',title: 'Loan Payment Approval' }
  ]);

  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };

  const [refreshing, setRefreshing] = React.useState(false);

  const ShowMerchant = async() => {
    let token = await AsyncStorage.getItem('@token');
    getMerchant(token).then((result)=>{
      setListMerchant(result.data.data.map(function(item){
        return ({
          id: item.id,
          name: item.name,
          phone: item.phone,
          address: item.address,
          photo: item.photo,
          cashbalance: item.cashBalance,
          verificationStatus: item.verificationStatus
        });
      }
      ));
    }).catch((err)=>{
      // console.log(err)
    });
  };

  const ShowLoan = async() => {
    let token = await AsyncStorage.getItem('@token');
    getLoan(token).then((result)=>{
      setListLoan(result.data.data.map(function(item){
        return ({
          id: item.id,
          name: item.customer.name,
          status: item.status,
          phone: item.customer.phone,
          photo: item.customer.photo,
          currentBalance: item.currentBalance,
          installment: item.installment,
          paymentProof: item.paymentProof
        });
      }
      ));
      console.log(result);
    }).catch((err)=>{
      console.log(err);
    });
  };

  const ShowPaymentApproval = async() => {
    let token = await AsyncStorage.getItem('@token');
    getPaymentApproval(token).then((result)=>{
      setListPayment(result.data.data.map(function(item){
        return ({
          id: item.id,
          status: item.status,
          name: item.customer.name,
          phone: item.customer.phone,
          photo: item.customer.photo,
          payment: item.paymentNominal,
          status: item.paymentStatus,
          paymentProof: item.paymentProof
        });
      }
      ));
      console.log(result.data.data);
    }).catch((err)=>{
      console.log(err);
    });
  };

  const ShowLoanPayment = async() => {
    let token = await AsyncStorage.getItem('@token');
    getLoanPayment(token).then((result)=>{
      setListLoanPayment(result.data.data.map(function(item){
        return ({
          id: item.id,
          status: item.status,
          name: item.loan.customer.name,
          phone: item.loan.customer.phone,
          photo: item.loan.customer.photo,
          installment: item.loan.installment,
          status: item.loan.status,
          paymentProof: item.loan.paymentProof
        });
      }
      ));
      // console.log(result.data.data);
    }).catch((err)=>{
      console.log(err);
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    ShowMerchant();
    ShowLoan();
    ShowPaymentApproval();
    ShowLoanPayment();
    wait(5000).then(() => setRefreshing(false));
  }, []);

  useEffect(()=>{
    ShowMerchant();
    ShowLoan();
    ShowPaymentApproval();
    ShowLoanPayment();
  },[]);

  const MerchantRoute = () => {
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />}>
        <View style={{ flex: 1,flexDirection: 'column' }}>
          { listMerchant.map((item,key)=>(

            <View key={key} style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

              <Image source={{ uri: item.photo }} style={{ width: 78,height: 78,flex: 3 }}/>
              <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
                <Text style={{ fontSize: 15,color: '#000000',marginVertical: 2 }}>{item.name}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{item.verificationStatus}</Text>
              </View>
              <CustomButton
                text="Manage"
                onPress = {()=>HandleManageClick(item.id,item.name,item.photo,item.verificationStatus,item.paymentProof)}
              />
              {/* <Button  style={{marginVertical:10,color:'black',backgroundColor:'#ffa500',flex:2,widht:70,height:40}} mode="contained">Manage</Button>		 	      */}
            </View>
          ))}

          <View style={{ flex: 5 }} />
        </View>
      </ScrollView>
    );
  };

  const LoanRoute = () => {
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />}>
        <View style={{ flex: 1,flexDirection: 'column' }}>
          {listLoan.map((item,key)=>(
            <View key={key} style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

              <Image source={{ uri: item.photo }} style={{ width: 78,height: 78,flex: 3 }}/>
              <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
                <Text style={{ fontSize: 15,color: '#000000',marginVertical: 2 }}>{item.name}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{'Rp ' + item.installment + '.00'}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{item.status}</Text>
              </View>
              <CustomButton onPress = {()=>HandleLoanClick(item.id,item.name,item.installment,item.photo,item.paymentProof)} text="Manage" textColor="#FFFFFF"/>
            </View>
          ))}
        </View>
      </ScrollView>

    );
  };

  const PaymentApprovalRoute = () => {
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />}>
        <View style={{ flex: 1,flexDirection: 'column' }}>
          {listPayment.map((item,key)=>(
            <View key={key} style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

              <Image source={{ uri: item.photo }} style={{ width: 78,height: 78,flex: 3 }}/>
              <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
                <Text style={{ fontSize: 15,color: '#000000',marginVertical: 2 }}>{item.name}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{'Rp ' + item.payment + '.00'}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{item.status}</Text>
              </View>
              <CustomButton onPress = {()=>HandlePaymentClick(item.id,item.name,item.payment,item.photo,item.paymentProof)} text="Manage" textColor="white" />


            </View>
          ))}
        </View>
      </ScrollView>

    );
  };

  const LoanPaymentRoute = () => {
    return (
      <ScrollView refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />}>
        <View style={{ flex: 1,flexDirection: 'column' }}>
          {listLoanPayment.map((item,key)=>(
            <View key={key} style={{ marginTop: 15,backgroundColor: '#EEEEEE', flex: 2, flexDirection: 'row' }}>

              <Image source={{ uri: item.photo }} style={{ width: 78,height: 78,flex: 3 }}/>
              <View style={{ marginVertical: 5, marginHorizontal: 7,flex: 6, flexDirection: 'column' }}>
                <Text style={{ fontSize: 15,color: '#000000',marginVertical: 2 }}>{item.name}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{'Rp ' + item.installment + '.00'}</Text>
                <Text style={{ fontSize: 13,color: '#666666',marginVertical: 2 }}>{item.status}</Text>
              </View>
              <CustomButton onPress = {()=>HandleLoanPaymentClick(item.id,item.name,item.installment,item.photo,item.paymentProof)} text="Manage" textColor="white" />


            </View>
          ))}
        </View>
      </ScrollView>

    );
  };

  const renderTabBar = propstab => {
    return (
      <TabBar
        {...propstab}
        indicatorStyle={{ backgroundColor: 'orange' }}
        style={{ backgroundColor: 'white' }}
        renderLabel={({ route, focused, color }) => (
          <Text style={{ color: '#666666', fontSize: 14 }}>
            {route.title}
          </Text>
        )}
      />
    );
  };

  const HandleManageClick = (id,name,photo,verificationStatus,) => {
    navigation.navigate('ManageMerchant',{ id: id,name: name,photo: photo,verificationStatus: verificationStatus });
  };

  const HandleLoanClick = (id,name,installment,photo,paymentProof) => {
    navigation.navigate('LoanDetailPage',{ id: id,name: name,installment: installment,photo: photo,paymentProof: paymentProof });
  };

  const HandlePaymentClick = (id,name,payment,photo,paymentProof) => {
    navigation.navigate('PaymentDetailPage',{ id: id,name: name,payment: payment,photo: photo,paymentProof: paymentProof });
  };

  const HandleLoanPaymentClick = (id,name,installment,photo,paymentProof) => {
    navigation.navigate('LoanPaymentPage',{ id: id,name: name,installment: installment,photo: photo,paymentProof: paymentProof });
  };

  const HandleLogOut = async () => {
    try {
      await AsyncStorage.removeItem('@token');
      await AsyncStorage.removeItem('@isAdmin');
      navigation.replace('SignIn');
    } catch (error) {
      showMessage('Error Logout', error.response);
    }
  };

  const renderScene = SceneMap({
    merchant: MerchantRoute,
    loan: LoanRoute,
    paymentapproval: PaymentApprovalRoute,
    loanpayment: LoanPaymentRoute
  });


  return (
    <SafeAreaView style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 5 }}>
      <View style={{ flex: 1,flexDirection: 'column',marginHorizontal: 5,marginVertical: 10 }}>
        <Title onPress={()=>ShowMerchant()} style={{ fontSize: 25 }}>Administration</Title>
        <Subheading style={{ marginBottom: 14 }}>You are now an admin</Subheading>
        <CustomButton textColor="#FFFFFF" onPress={()=>HandleLogOut()} text="Log Out"/>
        {/* <Searchbar placeholder="Search" style={{marginBottom:21}} /> */}
        <TabView renderTabBar={renderTabBar} navigationState={{ index, routes }} renderScene={renderScene} onIndexChange={setIndex} initialLayout={initialLayout} style={{ fontSize: 40 }}/>
      </View>
    </SafeAreaView>
  );
};

export default MerchantMain;

