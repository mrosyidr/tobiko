/* eslint-disable no-console */
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Config from 'react-native-config';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { ProfileDummy } from '../../assets';
import { getToken, showMessage, storeData } from '../../utils';
import MerchantHome from '../MerchantHome';
import { CustomButton, Gap } from '../../components';


const ProfileMerchant = ({ navigation }) => {
  const [userProfile, setUserProfile] = useState({});
  const [url, setUrl] = useState('');
  useEffect(() => {
    updateUserProfile();
  }, []);

  const updateUserProfile = () => {
    getToken().then((res) => {
      axios(Config.BASE_URL + '/api/merchant/profile', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'x-api-key': Config.API_KEY,
          'Authorization': 'Bearer ' + res
        }
      }).then((response) => {
        setUserProfile(response.data.data);
        setUrl(response.data.data.merchants[0].photo);
      }).catch((err) => {
        console.log('Errornya : ', err);
      });
    });
  };

  const handleLogout = async () => {
    try {
      await AsyncStorage.removeItem('@token');
      await AsyncStorage.removeItem('@isAdmin');
      await AsyncStorage.removeItem('@isMerchant');
      navigation.replace('SignIn');
    } catch (error) {
      showMessage('Error Logout', error.response);
    }
  };

  return (
    <View style={styles.page}>
      <View style={styles.profileDetail}>
        <View style={styles.photo}>
          <TouchableOpacity>
            <View style={styles.borderPhoto}>
              {url ? <Image source={{ uri: url }} style={styles.photoContainer} /> : <Image source={ProfileDummy} style={styles.photoContainer} />}
              {/* <Image
                source={ProfileDummy }
                style={styles.photoContainer}
              /> */}
            </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.name}>{userProfile.name}</Text>
        <Text style={styles.email}>{userProfile.email}</Text>
      </View>
      <View style={styles.content}>
        {/* <ProfileTabSection /> */}
        <View style={{ alignItems: 'center' }}>
          <CustomButton text="Back" onPress={()=>navigation.navigate('MerchantHome')} width={100}/>
          <Gap height={20}/>
          <CustomButton text="Logout" onPress={handleLogout} width={100}/>
        </View>
      </View>
    </View>
  );
};

export default ProfileMerchant;

const styles = StyleSheet.create({
  page: { flex: 1 },
  content: { flex: 1, marginTop: 24 },
  profileDetail: { backgroundColor: 'white', paddingBottom: 26 },
  name: {
    fontSize: 18,
    fontFamily: 'Poppins-Medium',
    color: '#020202',
    textAlign: 'center'
  },
  email: {
    fontSize: 13,
    fontFamily: 'Poppins-Light',
    color: '#8D92A3',
    textAlign: 'center'
  },
  photo: { alignItems: 'center', marginTop: 26, marginBottom: 16 },
  borderPhoto: {
    borderWidth: 1,
    borderColor: '#8D92A3',
    width: 110,
    height: 110,
    borderRadius: 110,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photoContainer: {
    width: 90,
    height: 90,
    borderRadius: 90,
    backgroundColor: '#F0F0F0',
    padding: 24
  }
});
