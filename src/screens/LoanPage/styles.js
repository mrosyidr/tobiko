import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  // h1:{
  //     fontSize: 22,
  //     marginBottom : 5
  // },
  // h2:{
  //     fontSize: 20,
  //     marginBottom : 5
  // },
  // h3:{
  //     fontSize: 17,
  //     marginBottom : 5
  // },
  // h4:{
  //     fontSize: 14,
  //     marginBottom : 5
  // },
  // modal:{
  //     left: '5%',
  //     top: '35%',
  //     height: '37%',
  //     width: '90%',
  //     backgroundColor:'white',
  //     // borderColor: 'grey',
  //     shadowColor: "#202020",
  //     shadowOffset: {
  //         width: 0,
  //         height: 2,},
  //     flexDirection: 'column',
  //     borderRadius: 30,
  //     shadowOpacity: 0.5,
  //     shadowRadius: 3.84,
  //     elevation: 5,
  //     // borderWidth: 0.3,
  // },
  // modal2:{
  //     marginTop: 10,
  //     marginRight: 15,
  //     alignItems:'flex-end',
  //     justifyContent:'flex-start',
  // },
  // totalloan:{
  //     marginBottom: 0,
  //     backgroundColor:'#f2f2f2',
  //     flex:0.1,
  //     flexDirection:'column',
  //     justifyContent:'space-around',
  //     alignItems:'flex-start'
  // }

  h1: {
    fontSize: 22,
    marginBottom: 5
  },
  h2: {
    fontSize: 20,
    marginBottom: 5
  },
  h3: {
    fontSize: 17,
    marginBottom: 5
  },
  h4: {
    fontSize: 14,
    marginBottom: 5
  },
  modal: {
    left: '5%',
    top: '35%',
    height: '37%',
    width: '90%',
    backgroundColor: 'white',
    // borderColor: 'grey',
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2 },
    flexDirection: 'column',
    borderRadius: 30,
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5
    // borderWidth: 0.3,
  },
  modal2: {
    marginTop: 10,
    marginRight: 15,
    alignItems: 'flex-end',
    justifyContent: 'flex-start'
  },
  totalloan: {
    backgroundColor: '#f2f2f2',
    flex: 0.2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default styles;
