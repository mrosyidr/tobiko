import React, { useState } from 'react';
import { FlatList, View, Text, TouchableOpacity, Image } from 'react-native';
import { Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/Ionicons';

import { CustomButton, Header } from '../../components';

function AdminPaymentLoan(props){
  const [refreshing] = useState(true);
  const [selectedId, setSelectedId] = useState(1);
  const [] = useState([]);
  const [adminTab] = useState([
    {
      id: 1,
      name: 'Approval'
    },
    {
      id: 2,
      name: 'Reports'
    }
  ]);
  const [] = useState([
    {
      'id': 1,
      'price': 12000000,
      'name': 'MyAds',
      'ingredients': 'Mengirim pesan iklan Anda ke lokasi yang tepat dalam bentuk teks atau gambar.',
      'description': 'Kirim iklan promosi Anda dalam SMS, MMS dan pesan Pop-up ke lebih dari 160 juta pengguna Telkomsel',
      'favorite': false,
      'img': 'https://www.telkomsel.com/sites/default/files/asset/fullhtml/img/gallery/telkomsel/header_myads.jpeg',
      'recommended': false,
      'hottest': false,
      'popular': false,
      'newProduct': true,
      'top': false,
      'average_rating': 3.5
    },
    {
      'id': 2,
      'price': 12000000,
      'name': 'IoT Managed Service Connectivity',
      'ingredients': 'Penuhi kebutuhan bisnis Anda dengan satelit broadband, akses Internet, dan layanan video streaming.',
      'description': 'Kelola aset bisnis Anda dengan managed connectivity services yang handal.',
      'favorite': false,
      'img': 'https://www.telkomsel.com/sites/default/files/asset/fullhtml/img/gallery/telkomsel/header_IoT-Manage-Service-Connectivity.jpg',
      'recommended': false,
      'hottest': true,
      'popular': true,
      'newProduct': false,
      'top': false,
      'average_rating': 4.1
    },
    {
      'id': 3,
      'price': 12000000,
      'name': 'NB-IoT',
      'ingredients': 'Baterai tahan lama hingga 10 tahun \n\n+ 20dB link budget (vs. GSM)',
      'description': 'Teknologi jaringan LPWAN yang dirancang spesifik untuk mendukung perangkat IoT dengan efisiensi tinggi.',
      'favorite': true,
      'img': 'https://www.telkomsel.com/sites/default/files/asset/fullhtml/img/gallery/telkomsel/header_nb_iot.jpg',
      'recommended': false,
      'hottest': true,
      'popular': true,
      'newProduct': false,
      'top': false,
      'average_rating': 5.0
    }
  ]);

  const [loanList] = useState([
    {
      id: 1,
      name: 'Payment Loan 1',
      totalLoan: 18000000,
      status: 'Paid',
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 2,
      name: 'Payment Loan 2',
      totalLoan: 21000000,
      status: 'On Going',
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    },
    {
      id: 3,
      name: 'Payment Loan 3',
      totalLoan: 21000000,
      status: 'Paid',
      avatar: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg'
    }
  ]);



  //  useEffect(() => {
  //     handleGetCategory(1)
  // }, [])

  const Item = ({ item, onPress, color, div, style, ...others }) => (
    <TouchableOpacity
      onPress={onPress}
      style={[
        { height: 50, backgroundColor: '#FFFFFF', marginRight: 24 },
        style
      ]}>
      <View
        style={{
          flex: 1,
          alignContent: 'center',
          justifyContent: 'center'
        }}>
        <Text
          style={[
            {
              lineHeight: 32,
              fontWeight: '500',
              letterSpacing: -0.02,
              fontSize: 16,
              color: '#020202'
            },
            style
          ]}
          {...others}>
          {item.name}
        </Text>
        {/* <Divider style={{ backgroundColor: '#FFA451', height : {div}, width : 30}} /> */}

      </View>
    </TouchableOpacity>
  );

  const renderButton = ({ item }) => {
    const color = item.id === selectedId ? '#27214D' : '#938DB5';
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          //   handleGetCategory(item.id)
          //   toTop()
        }}
        style={{ color }}
      />
    );
  };


  const renderLoanList = ({ item }) => {
    return (
      <View style={{ flex: 1, width: '100%',
        height: 80, backgroundColor: '#FFFFFF',alignContent: 'center', justifyContent: 'center', marginTop: 3, marginBottom: 3 }} onPress={() => {
        // eslint-disable-next-line no-console
        console.log('yang dibuka dari onPress: ', item.id);
        // moveScreen({id : item.id})
      }}>
        <View style={{ paddingHorizontal: 24, flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={{ uri: item.avatar }}
            style={{ height: 65, width: 65, borderRadius: 8 }}
          />
          <View style={{ flexDirection: 'row', marginLeft: 5, justifyContent: 'space-between', width: '85%' }}>
            <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
              <Text style={{ fontSize: 16, fontWeight: '400', color: '#020202' }}>
                {item.name}
              </Text>
              <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                            IDR {item.totalLoan}
              </Text>
            </View>
            <View style={{ alignItems: 'center', width: '30%' }}>
              <Icons name={item.status === 'Paid' ? 'checkmark-circle' : 'close-circle'} color={item.status === 'Paid' ? '#00FF00' : '#FF0000'} size={30}/>
              <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                {item.status}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };


  return (
    <View style={{ flex: 1 }}>
      <View style={{ height: '15%', backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
        <Header title="Administration" subTitle="Verification payment loan from customer" />
      </View>

      <View style={{ width: '100%', backgroundColor: '#FFFFFF', paddingHorizontal: 24, height: '6%', marginTop: 24 }}>
        <View style={{ height: 50, flex: 1, alignContent: 'center' }}>
          <FlatList
            data = {adminTab}
            showsHorizontalScrollIndicator={false}
            horizontal = {true}
            renderItem={renderButton}
            keyExtractor={(item) => item.id.toString()}
            refreshing={refreshing}
            extraData={selectedId}
            // onRefresh={getData}
          />
        </View>
      </View>
      <Divider style={{ backgroundColor: '#F2F2F2', height: 1, width: '100%' }} />
      <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
        {selectedId === 1 ?
          <View style={{ flex: 1 }}>
            <View style={{ paddingHorizontal: 24, flexDirection: 'row', marginTop: 16,alignItems: 'center' }}>
              <Image
                source={{ uri: 'https://www.goarabic.com/vm/wp-content/uploads/2019/05/dummy-profile-pic.jpg' }}
                style={{ height: 80, width: 80, borderRadius: 8 }}
              />
              <Text style={{ color: '#020202', fontFamily: 'Poppins-Light', fontWeight: '400', fontSize: 16, marginLeft: 16 }}>
                        Customer A
              </Text>
            </View>
            <View style={{ marginTop: 24 }}>
              <Text style={{ color: '#020202', fontFamily: 'Poppins-Light', fontWeight: '400', fontSize: 16, marginLeft: 16, marginBottom: 4 }}>
                            Bukti Pembayaran
              </Text>
              <Text style={{ color: '#020202', fontFamily: 'Poppins-Light', fontWeight: '400', fontSize: 16, marginLeft: 16, marginBottom: 4 }}>
                            Total Loan
              </Text>
              <Text style={{ color: '#020202', fontFamily: 'Poppins-Light', fontWeight: '400', fontSize: 16, marginLeft: 16 }}>
                            IDR 2000,000
              </Text>
            </View>
            <View style={{ marginTop: 24, marginHorizontal: 100 }}>
              <CustomButton text="Approve" onPress={() =>
                // eslint-disable-next-line no-alert
                alert('are you sure want to approve?')
              } />
            </View>

          </View>
          : <View style={{ flex: 1 }}>
            <View style={{ marginTop: 24, flex: 1 }}>
              <Text style={{ color: '#020202', fontFamily: 'Poppins-Light', fontWeight: '400', fontSize: 16, marginLeft: 16 }}>
                                Loan Payment List
              </Text>
              <View style={{ flex: 1 }}>
                <FlatList
                  // ref={flatListRef}
                  data = {loanList}
                  showsVerticalScrollIndicator={false}
                  // showsHorizontalScrollIndicator={false}
                  // horizontal = {true}
                  renderItem={renderLoanList}
                  keyExtractor={(item) => item.id.toString()}
                  refreshing={refreshing}
                  // extraData={selectedId}
                  // onRefresh={getData}
                />
              </View>
            </View>

          </View>
        }
      </View>
    </View>
  );
}

export default AdminPaymentLoan;
