/* eslint-disable no-console */
import React, { useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Config from 'react-native-config';

import { setLoading } from '../../redux/actions';
import { CustomButton, Gap, Header, CustomTextInput } from '../../components';
import { showMessage, useForm, getToken } from '../../utils';

const SignIn = ({ navigation }) => {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    'email': '',
    'password': ''
  });

  const processToken = async () => {
    const token = await getToken();
    const isAdminToken = await AsyncStorage.getItem('@isAdmin');
    const isMerchantToken = await AsyncStorage.getItem('@isMerchant');
    if (token) {
      if (isAdminToken === 'true') {
        navigation.replace('MerchantMain');
      }
      if (isMerchantToken === 'true') {
        navigation.replace('MerchantHome');
      }
      navigation.replace('HomePage');
    }
  };

  useEffect(() => {
    processToken();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    return () => {
      console.log('-- SignIn components cleanup and unmounted');
    };
  }, []);

  const storeData = async (value, isAdmin, isMerchant) => {
    try {
      await AsyncStorage.setItem('@token', value);
      await AsyncStorage.setItem('@isAdmin', isAdmin);
      await AsyncStorage.setItem('@isMerchant', isMerchant);
    } catch (err) {
      showMessage('Failed to store token:', err.response);
    }
  };

  const submitLogin = async (form) => {
    dispatch(setLoading(true));
    await axios(Config.BASE_URL + '/api/auth', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY
      },
      data: form
    }).then((response) => {
      showMessage(response.data.message, 'success');
      dispatch(setLoading(false));
      if (response.data.data.isAdmin) {
        storeData(response.data.data.token, JSON.stringify(response.data.data.isAdmin),'');
        navigation.replace('MerchantMain');
      } else if (response.data.data.isMerchant) {
        storeData(response.data.data.token,'',JSON.stringify(response.data.data.isMerchant));
        navigation.replace('MerchantHome');
      } else {
        storeData(response.data.data.token,'','');
        navigation.replace('HomePage');
      }
    }).catch((err) => {
      showMessage('Login is Failed:', err.response);
      dispatch(setLoading(false));
    });
  };

  const onSubmit = () => {
    if ((form.email) && (form.password)) {
      submitLogin(form);
    }
  };


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={styles.page}>
        <Header title="Sign In" subTitle="One step more to buy products" />
        <View style={styles.container}>
          <CustomTextInput
            label="Email Address"
            placeholder="Type your email address"
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={16} />
          <CustomTextInput
            label="Password"
            placeholder="Type your password"
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
            secureTextEntry
          />
          <Gap height={24} />
          <CustomButton text="Sign In" onPress={onSubmit} />
          <Gap height={12} />
          <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
            <Text style={{ textAlign: 'center', fontFamily: 'Poppins-Medium', fontSize: 14 }}>Forgot password?</Text>
          </TouchableOpacity>
          <Gap height={24} />
          <CustomButton
            text="Continue without SignIn"
            color="#8D92A3"
            textColor="white"
            onPress={() => navigation.navigate('HomePage')}
          />
          <Gap height={24} />
          <CustomButton
            text="Create New Account"
            color="#8D92A3"
            textColor="white"
            onPress={() => navigation.navigate('SignUp')}
          />
          <Gap height={24} />
          <CustomButton
            text="Create New Merchant"
            color="#8D92A3"
            textColor="white"
            onPress={() => navigation.navigate('SignUpMerchant')}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  page: { flex: 1, backgroundColor: '#FAFAFC' },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 26,
    marginTop: 24,
    flex: 1
  }
});
