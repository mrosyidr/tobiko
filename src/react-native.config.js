import { NativeModules } from "react-native";

NativeModules.exports = {
    project: {
        ios: {},
        android: {},
    },
    assets: ['./src/assets/fonts'],
};