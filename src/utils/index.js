import useForm from './useForm';
import fetchProduct from './fetchProduct';
export * from './showMessage';
// export * from './storage';
// export * from './fetchApi'
import fetchApi from './fetchApi';
//import storage from './storage';
export * from './showMessage';
export * from './storage';
// export * from './fetchApi';
export * from './getToken';

export { useForm, fetchApi, fetchProduct };

// import useForm from './useForm';
// import fetchProduct from './fetchProduct'
// import fetchApi from './fetchApi'
// import {storeData, getData} from './storage'
// import showMessage from './showMessage'
// import getToken from './getToken'

// export {useForm, fetchApi, fetchProduct, storeData, getData, showMessage, getToken};
