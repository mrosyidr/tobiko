/* eslint-disable prefer-destructuring */
/* eslint-disable no-console */
import messaging from '@react-native-firebase/messaging';
import { Platform } from 'react-native';

class FCMService {

    register = (onRegister, onNotification, onOpenNotification) => {
      this.checkPermission(onRegister);
      this.createNotificationListeners(onRegister, onNotification, onOpenNotification);
    }

    registerAppWithFCM = async() => {
      if (Platform.OS === 'ios'){
        await messaging().registerDeviceForRemoteMessages();
        await messaging().setAutoInitEnabled(true);
      }
    }

    checkPermission = (onRegister) => {
      messaging().hasPermission()
        .then(enabled => {
          if (enabled){
            this.getToken(onRegister);
          } else {
            this.requestPermission(onRegister);
          }
        }).catch(error => {
          console.log('[FCMService] Permission Rejected', error);
        });
    }

    getToken = (onRegister) => {
      messaging().getToken()
        .then(fcmToken => {
          if (fcmToken) {
            onRegister(fcmToken);
          } else {
            console.log('[FCMService] User does not have a device token');
          }
        }).catch(error => {
          console.log('[FCMService] getToken Rejected', error);
        });
    }

    requestPermission = (onRegister) => {
      messaging().requestPermission()
        .then(() => {
          this.getToken(onRegister);
        }).catch(error => {
          console.log('[FCMService] Request Permission Rejected', error);
        });
    }

    deleteToken = () => {
      console.log('[FCMService] delete Token');
      messaging().deleteToken()
        .catch(error => {
          console.log('[FCMService] Delete Token Error', error);
        });
    }

    createNotificationListeners = (onRegister, onNotification, onOpenNotification) => {
      //When the Application is running, but in the background
      messaging()
        .onNotificationOpenedApp(remoteMessage => {
          console.log('[FCMService] onNotificationOpenedApp Notification caused App to Open from background state', remoteMessage);
          if (remoteMessage) {
            const notification = remoteMessage.notification;
            onOpenNotification(notification);
          }
        });

      //When the Application is opened from a quit state
      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          console.log('[FCMService] getInitialNotification Notification caused App to Open from quit state', remoteMessage);
          if (remoteMessage){
            const notification = remoteMessage.notification;
            onOpenNotification(notification);
          }
        });

      //Foreground state message
      this.messageListener = messaging().onMessage(async remoteMessage => {
        console.log('[FCMService] A New FCM message arrived', remoteMessage);
        if (remoteMessage) {
          let notification = null;
          if (Platform.OS === 'ios'){
            notification = remoteMessage.data.notification;
          } else {
            notification = remoteMessage.notification;
          }
          onNotification(notification);
        }
      });

      //Triggered when have new token
      messaging().onTokenRefresh(fcmToken => {
        console.log('[FCMService] New Token Refresh', fcmToken);
        onRegister(fcmToken);
      });
    }

    unRegister = () => {
      this.messageListener();
    }
}

export const fcmService = new FCMService();
