import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../showMessage/index';

async function fetchProduct(method, auth, path, id, page, word, body){
  try {
    const response = await axios({ method,
      url: `${Config.BASE_URL}${path}${id}?page=${page}&search=${word}`,
      headers: { 'x-api-key': `${Config.API_KEY}`,
        'Authorization': `Bearer ${auth}` },
      data: body });
    if (response.status !== 400){
      return response;
    }
  } catch (error){
    // throw error;
    showMessage(error.message, 'warning');
  }
}

export default fetchProduct;
