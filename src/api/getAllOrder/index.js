import { fetchApi, showMessage } from '../../utils/';

//FOR GET ALL ORDER

async function getAllOrder(auth){
  try {
    const result = await fetchApi('GET',auth,'/api/order',null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getAllOrder;
