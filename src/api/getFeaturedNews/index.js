import { fetchApi, showMessage } from '../../utils/';

//FOR GET FEATURED NEWS

async function getFeaturedNews(){
  try {
    const result = await fetchApi('GET','','/api/news/featured',null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getFeaturedNews;
