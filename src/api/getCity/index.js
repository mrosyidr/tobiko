import { fetchApi, showMessage } from '../../utils';

//FOR GET CITY

async function getCity(id){
  try {
    const result = await fetchApi('GET', '',`/api/data/city/${id}`, null);
    if (result.data.code === 200){
      // showMessage(result.data.message, 'success');
      return result;
    } else {
      showMessage(result.data.message, 'warning');
    }
  } catch (error){
    showMessage(error.message, 'warning');
    // throw error;
  }
}

export default getCity;
