import { fetchProduct, showMessage } from '../../utils/';

//FOR POST PRODUCT TO CART

async function postProductToCart(token, merchant, id, quantity){
  try {
    const result = await fetchProduct('POST', token,'/api/cart','','','',{
      'merchantId': merchant,
      'productId': id,
      'qty': quantity
    });
    // console.log(result);
    return result;
  } catch (error){
    // showMessage(error.message, 'warning');
    showMessage('Your Session Expired, Please Login', 'warning');
    throw error;
  }
}

export default postProductToCart;
