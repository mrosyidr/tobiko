import axios from 'axios';
import Config from 'react-native-config';

export const getShippingData = async (merchantCity, myAddressCityId, totalWeight, shippingPartnerValue) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/data/shipping', {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY
      },
      data: {
        originCityId: merchantCity,
        destinationCityId: myAddressCityId,
        weight: totalWeight,
        shippingPartner: shippingPartnerValue
      }
    });
    if (response.status === 200){
      return response.data.data;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
