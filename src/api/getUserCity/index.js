import { fetchApi } from '../../utils';

export const getUserCity = async (auth) => {
  try {
    const response = await fetchApi('GET', auth, '/api/user/profile', null);
    if (response.status === 200) {
      return response.data.data.city.name;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
