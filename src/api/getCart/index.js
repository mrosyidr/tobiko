import { fetchApi } from '../../utils';

export const getCart = async (auth) => {
  try {
    const response = await fetchApi('GET', auth, '/api/cart', null);
    if (response.status === 200){
      return response.data.data.cart;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
