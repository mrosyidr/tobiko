import axios from 'axios';
import Config from 'react-native-config';

const getLoanPayment = async (auth) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/admin/payment/loan', {
      method: 'GET',
      headers: {
        'x-api-key': Config.API_KEY,
        'Authorization': `Bearer ${auth}`,
        'Content-Type': 'application/json'
      }
    });
    if (response.status !== 400){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};

export default getLoanPayment;
