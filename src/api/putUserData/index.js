import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

export const putUserData = async (auth, body) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/user/profile', {
      method: 'PUT',
      headers: {
        'x-api-key': Config.API_KEY,
        'Authorization': `Bearer ${auth}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      data: {
        'name': body.name,
        'password': body.password,
        'email': body.email
      }
    });
    if (response.status !== 400){
      showMessage('Edit Profille Success', 'success');
      console.log(response);
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
    showMessage('Edit Profile Failed', 'danger');
  }
};

// export default putUserData;
