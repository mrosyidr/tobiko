import axios from 'axios';
import Config from 'react-native-config';

export const postLoanMidtrans = async (loanid, nominal, auth) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/payment/loan/midtrans', {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth}`
      },
      data: {
        'loanId': loanid,
        'nominal': nominal
      }
    });
    if (response.status !== 400){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
