import { fetchApi } from '../../utils';

export const postOrder = async (auth, order) => {
  try {
    const response = await fetchApi('POST', auth, '/api/order', order);
    if (response.status === 200){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
