import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

export const postPayOrderTransfer = async (orderId, nominal, image64, auth) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/payment/trx/transfer', {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY,
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth}`
      },
      data: {
        'orderId': orderId,
        'paymentProof': `data:${image64.type};base64,${image64.base64}`,
        'total': nominal
      }
    });
    if (response.status !== 400){
      showMessage('Bank Transfer Payment Success !!', 'success');
      return response;
    }
  } catch (error) {
    showMessage('Bank Transfer Payment Failed !!', 'danger');
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
