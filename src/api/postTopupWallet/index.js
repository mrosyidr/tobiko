import axios from 'axios';
import Config from 'react-native-config';

import { showMessage } from '../../utils';

export const postTopupWallet = async (auth, nominal) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/topup', {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY,
        'Authorization': `Bearer ${auth}`,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      data: {
        'nominal': nominal
      }
    });
    if (response.status !== 400){
      showMessage('Top Up Wallet Success', 'success');
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
    showMessage('Top Up Wallet Failed', 'danger');
  }
};
