import axios from 'axios';
import Config from 'react-native-config';

export const getMerchantCity = async (merchantId) => {
  try {
    const response = await axios(Config.BASE_URL + '/api/data/merchant/' + merchantId, {
      method: 'GET',
      headers: {
        'x-api-key': Config.API_KEY
      }
    });
    if (response.status === 200){
      return response.data.data[0].cityId;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
