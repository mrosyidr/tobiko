import axios from 'axios';
import Config from 'react-native-config';

export const postLoan = async (auth, orderId, totalPrice) => {
  try {
    const response = await axios(Config.BASE_URL + '/api/loan/apply', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization':  `Bearer ${auth}`
      },
      data: {
        orderId: orderId,
        nominal: totalPrice,
        tenor: 1
      }
    });
    if (response.status === 200){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};
