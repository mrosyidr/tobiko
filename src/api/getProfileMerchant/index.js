import axios from 'axios';
import Config from 'react-native-config';

import { getToken, showMessage, setUri } from '../../utils';


export const getProfileMerchant = () => {
  getToken().then((res) => {
    axios(Config.BASE_URL + '/api/merchant/profile', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': Config.API_KEY,
        'Authorization': 'Bearer ' + res
      }
    }).then((response) => {
      setUri(response.data.data.merchants[0].photo);
    }).catch((err) => {
      showMessage(`Error: ${err}`);
    });
  });
};
