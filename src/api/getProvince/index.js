import { fetchApi, showMessage } from '../../utils';

//FOR GET PROVINCE

async function getProvince(path){
  try {
    const result = await fetchApi('GET', '',path, null);
    if (result.data.code === 200){
      // showMessage(result.data.message, 'success');
      return result;
    } else {
      showMessage(result.data.message, 'warning');
    }
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getProvince;
