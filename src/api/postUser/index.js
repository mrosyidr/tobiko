import fetchApi from '../../utils';

//FOR LOGIN

async function postUser(path, body){
  try {
    const result = await fetchApi('POST', '', path, body);
    // eslint-disable-next-line no-console
    console.log(result);
    return result;
  } catch (error){
    throw error;
  }
}

export default postUser;
