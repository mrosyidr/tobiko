import { fetchApi, showMessage } from '../../utils/';

//FOR GET ALL NEWS

async function getAllNews(page){
  try {
    const result = await fetchApi('GET','','/api/news/list','/',page,'',null);
    // console.log(result);
    return result;
  } catch (error){
    showMessage(error.message, 'warning');
    throw error;
  }
}

export default getAllNews;
