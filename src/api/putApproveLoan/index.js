import axios from 'axios';
import Config from 'react-native-config';

const approveLoan = async (auth,id) => {
  try {
    let response = await axios(Config.BASE_URL + '/api/admin/loan/approve/' + id, {
      method: 'POST',
      headers: {
        'x-api-key': Config.API_KEY,
        'Authorization': `Bearer ${auth}`
      }
    });
    if (response.status !== 400){
      return response;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error.response);
  }
};

export default approveLoan;
