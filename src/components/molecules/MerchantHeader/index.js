import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';

import { ProfileDummy } from '../../../assets';

const MerchantHeader = ({ title, subTitle, onBack, url }) => {
  //console.log('Gambarnya', url);
  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subTitle}>{subTitle}</Text>
      </View>
      {onBack && (
        <TouchableOpacity activeOpacity={0.7} onPress={onBack}>
          <View style={styles.back}>
            {/* <Text style={{ fontFamily: 'Poppins-Medium', fontWeight: 'bold', fontSize: 16 }}>Logout</Text> */}
            {url ? <Image source={{ uri: url }} style={{ width: 50, height: 50 }} /> : <Image source={ProfileDummy} style={{ width: 50, height: 50 }} />}
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default MerchantHeader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingTop: 30,
    paddingBottom: 24,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: { fontSize: 22, fontFamily: 'Poppins-Medium', color: '#020202', fontWeight: 'bold' },
  subTitle: { fontSize: 14, fontFamily: 'Poppins-Light', color: '#8D92A3' },
  back: {
    marginRight: 16
  },
  imageProfileContainer: {
    backgroundColor: 'blue',
    padding: 16,
    marginRight: 10
  }
});
