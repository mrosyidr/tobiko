import React from 'react';
import { View, TouchableOpacity, Text, Image, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

function ProductList(props){
  const { OnPress, banner, name, price, averageRating, ...others } = props;
  return (
    <TouchableOpacity style={{ flex: 1,
      width:  Dimensions.get('window').width, height: 80, backgroundColor: '#FFFFFF',alignContent: 'center', justifyContent: 'center' }}
    onPress={OnPress}
    {...others}>
      <View style={{ paddingHorizontal: 24, flexDirection: 'row', justifyContent: 'space-between' }}>
        <Image
          source={{ uri: banner }}
          style={{ height: 65, width: 65, borderRadius: 8 }}
        />
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center', width: Dimensions.get('window').width * 0.4 }}>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#020202', fontFamily: 'Poppins-Medium' }}>
              {name}
            </Text>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
                            Rp {price}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 16 }}>
            <Icons name="star" size={16} color={averageRating >= 2 ? '#FFC700' : '#ECECEC'}/>
            <Icons name="star" size={16} color={averageRating >= 4 ? '#FFC700' : '#ECECEC'}/>
            <Icons name="star" size={16} color={averageRating >= 6 ? '#FFC700' : '#ECECEC'}/>
            <Icons name="star" size={16} color={averageRating >= 8 ? '#FFC700' : '#ECECEC'}/>
            <Icons name="star" size={16} color={averageRating >= 10 ? '#FFC700' : '#ECECEC'}/>
            <Text style={{ color: '#8D92A3', marginLeft: 8 }}>
              {averageRating}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default ProductList;
