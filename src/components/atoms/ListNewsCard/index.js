import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Image } from 'react-native';

function ListNewsCard(props){
  const { OnPress, banner, title, ...others } = props;
  return (
    <TouchableOpacity
      style={styles.card}
      onPress={OnPress}
      {...others}>
      <View style={styles.Image}>
        <Image
          source={{ uri: banner }}
          style={{ height: 65, width: 65, borderRadius: 8 }}
        />
        <View style={{ marginLeft: 12, width: '78%' }}>
          <Text style={styles.Text}>
            {title}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    width: 375,
    height: 95,
    backgroundColor: '#FFFFFF',
    alignContent: 'center',
    justifyContent: 'center',
    marginLeft: 8,
    marginRight: 8,
    borderRadius: 8,
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 16
  },
  Image: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignContent: 'center',
    alignItems: 'center'
  },
  Text: {
    fontSize: 14,
    fontWeight: '400',
    color: '#020202',
    fontFamily: 'Poppins-Regular'
  }
});

export default ListNewsCard;
