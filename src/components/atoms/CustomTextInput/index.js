import React from 'react';
import { View, TextInput, StyleSheet, Text } from 'react-native';

const CustomTextInput = ({ label, placeholder, value, onChangeText, isEmail = false, isNumber = false, ...props }) => {
  return (
    <View>
      <Text style={style.label}>{label}</Text>
      <TextInput style={style.input} placeholder={placeholder} value={value} onChangeText={onChangeText} {...props}/>
    </View>
  );
};
const style = StyleSheet.create({
  label: {
    fontSize: 14,
    marginBottom: 10,
    fontFamily: 'Poppins-Medium'
  },
  input: {
    backgroundColor: '#F3F1F1',
    borderRadius: 8,
    padding: 10,
    fontFamily: 'Poppins-Medium'
  }

});
export default CustomTextInput;
