import React from 'react';
import moment from 'moment';
import { View, TouchableOpacity, StyleSheet, Text, Image } from 'react-native';

function NewsCard(props){
  const { OnPress, banner, title, date, subtitle, ...others } = props;
  return (
    <View style={{ paddingHorizontal: 16, marginBottom: 12, marginTop: 12 }}>
      <TouchableOpacity onPress={OnPress} {...others}>
        <Image
          source={{ uri: banner }}
          resizeMode="cover"
          style={{ borderRadius: 8, width: '100%', height: 200 }}
        />
      </TouchableOpacity>
      <View style={{ flex: 1, marginTop: -40, height: '100%' }}>
        <TouchableOpacity style={styles.card} onPress={OnPress}>
          <Text style={styles.textDate}>
            {moment(date).format('MMM DD, YYYY')}
          </Text>
          <Text style={styles.textTitle}>
            {title}
          </Text>
          <Text style={styles.textContent}>
            {subtitle}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    shadowColor: '#202020',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 8
  },
  textTitle: {
    marginBottom: 10,
    marginTop: 10,
    color: '#020202',
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    paddingHorizontal: 16,
    lineHeight: 20
  },
  textContent: {
    marginBottom: 10,
    color: '#020202',
    fontFamily: 'Poppins-Light',
    fontSize: 14,
    paddingHorizontal: 16,
    lineHeight: 20
  },
  h1: {
    backgroundColor: '#FFFFFF',
    flex: 1
  },
  h2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF'
  },
  textDate: {
    fontSize: 14,
    paddingHorizontal: 16,
    fontFamily: 'Poppins-Regular',
    marginTop: 16,
    lineHeight: 16,
    color: 'red'
  }
});

export default NewsCard;
