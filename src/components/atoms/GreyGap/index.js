import React from 'react';
import { View, Dimensions } from 'react-native';

const GreyGap = ({ width = Dimensions.get('window').width, height }) => {
  return <View style={{ width: width, height: height, backgroundColor: '#f2f2f2' }} />;
};

export default GreyGap;
