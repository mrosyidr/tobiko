import React from 'react';
import { View, TouchableOpacity, Text, Image, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

function MyOrderCard(props){
  const { OnPress, banner, name, status, ...others } = props;
  return (
    <TouchableOpacity style={{ flex: 1,
      width: Dimensions.get('window').width, height: Dimensions.get('window').height * 0.1 ,
      backgroundColor: '#FFFFFF',alignContent: 'center', justifyContent: 'center' }}
    onPress={OnPress}
    {...others}>
      <View style={{ paddingHorizontal: 24, flexDirection: 'row' }}>
        <Image
          source={{ uri: banner }}
          style={{ height: 65, width: 65, borderRadius: 8 }}
        />
        <View style={{ flexDirection: 'row', justifyContent: 'space-between',alignItems: 'center', flex: 1 }}>
          <View style={{ marginLeft: 12, alignContent: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#020202', fontFamily: 'Poppins-Medium' }}>
              {name}
            </Text>
            <Text style={{ fontSize: 14, fontWeight: '400', color: '#8D92A3' }}>
              {status}
            </Text>
          </View>
          <Icons name="chevron-forward" size={40}/>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default MyOrderCard;
