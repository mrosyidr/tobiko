import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';

const Select = ({ label, value, onSelectChange, data }) => {


  var options = {
    '1': 'Jawa Barat',
    '2': 'Jawa Tengah',
    '3': 'Jawa Timur',
    '4': 'Sumatera Barat'
  };

  const [state, setState] = useState();

  state = {
    province: 'Jawa Barat'
  };

  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.input}>
        <Picker mode="dropdown" selectedValue={value}
          onValueChange={(value) => setState(value)}>
          {data.map((item) => {
            return (<Picker.Item label={item.name} value={item.name} key={item.id} />);
            //if you have a bunch of keys value pair
          })}
        </Picker>
      </View>
    </View>
  );
};

export default Select;

const styles = StyleSheet.create({
  label: { fontSize: 16, fontFamily: 'Poppins-Regular', color: '#020202' },
  input: {
    borderWidth: 1,
    borderColor: '#020202',
    borderRadius: 8,
    paddingHorizontal: 2,
    paddingVertical: 0
  }
});
