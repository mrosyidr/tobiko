import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Image, Dimensions } from 'react-native';
import Icons from 'react-native-vector-icons/Ionicons';

function LatestProductCard(props){
  const { OnPress, banner, name, category, averageRating, length, ...others } = props;
  return (
    <TouchableOpacity style={styles.card(length)}
      onPress={OnPress}
      {...others}>
      <View style={{ flex: 1 }}>
        <Image
          source={{ uri: banner }}
          style={styles.Image}
        />
        <View style={styles.labelNew}>
          <Text style={{ color: '#8D92A3', fontFamily: 'Poppins-Medium' }}>
                New
          </Text>
        </View>
        <View style={{ padding: 12 }}>
          <View style={{ height: 50 }}>
            <Text style={styles.text}>
              {name}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ color: '#8D92A3' }}>
                {category}  |
              </Text>
              <Icons name="star" size={16} color={averageRating >= 2 ? '#FFC700' : '#ECECEC'} style={{ marginLeft: 8 }}/>
              <Icons name="star" size={16} color={averageRating >= 4 ? '#FFC700' : '#ECECEC'}/>
              <Icons name="star" size={16} color={averageRating >= 6 ? '#FFC700' : '#ECECEC'}/>
              <Icons name="star" size={16} color={averageRating >= 8 ? '#FFC700' : '#ECECEC'}/>
              <Icons name="star" size={16} color={averageRating >= 10 ? '#FFC700' : '#ECECEC'}/>
              <Text style={{ color: '#8D92A3', marginLeft: 8 }}>
                {averageRating}
              </Text>
            </View>

          </View>

        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  card: (length) => (
    {
      width: length === 1 ? Dimensions.get('window').width * 0.9 : Dimensions.get('window').width * 0.8,
      marginLeft: length === 1 ? 16 : 8,
      marginRight: length === 1 ? 16 : 8,
      borderRadius: 8,
      backgroundColor: '#FFFFFF',
      shadowColor: '#202020',
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.5,
      shadowRadius: 3.84,
      elevation: 5,
      marginBottom: 16
    }),
  Image: {
    height: 120,
    width: '100%',
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8
  },
  labelNew: {
    backgroundColor: '#FFC700',
    position: 'absolute',
    top: 10,
    right: 10,
    alignSelf: 'flex-end',
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4
  },
  text: {
    fontSize: 16,
    color: '#020202',
    fontWeight: '400',
    fontFamily: 'Poppins-Regular'
  }
});

export default LatestProductCard;
