import React, { useEffect, useState } from 'react';

import { Provider } from 'react-redux';

import messaging from '@react-native-firebase/messaging';

import codePush from 'react-native-code-push';

import MainNavigation from '../src/navigation/MainNavigation';

import store from './redux/store';

import { ModalNotification } from './components/';

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};

const App = () => {
  const [modalNotificationVisible, setModalNotificationVisible] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [banner, setBanner] = useState('');
  // Mount the FCM From Server
  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      setModalNotificationVisible(true);
      setTitle(remoteMessage.notification.title);
      setContent(remoteMessage.notification.body);
      setBanner(remoteMessage.notification.android.imageUrl);
    });
    // Unmount FCM if done
    return unsubscribe;
  }, []);

  return (
    <Provider store={store}>
      <MainNavigation />
      <ModalNotification
        visible={modalNotificationVisible}
        title={title}
        content={content}
        banner={banner}
        buttonText="OK"
        buttonColor="#FFA451"
        textColor="#FFFFFF"
        onRequestClose={() => setModalNotificationVisible(!modalNotificationVisible)}
        onPress={() => {
          setModalNotificationVisible(!modalNotificationVisible);
        }}
      />
    </Provider>
  );
};

export default codePush(codePushOptions)(App);
