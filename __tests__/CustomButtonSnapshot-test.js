import React from 'react';
import renderer from 'react-test-renderer';

import CustomButton from '../src/components/atoms/CustomButton';

// Unit Test (Snapshot) for CustomButton component
it('renders correctly', () => {
  const tree = renderer.create(
    <CustomButton text="Test Button" width={163} height={45} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
