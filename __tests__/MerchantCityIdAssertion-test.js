/* eslint-disable no-console */
import { getMerchantCity } from '../src/api/getMerchantCity';

jest.setTimeout(120000); // for handling jest's async timeout default 5000ms (just in case, network connection is bad for getting API response)

/*
{
  Case: Unit Test (Assertion) for getMerchantCity hook component
  Objective: getting City Id of specified merchant (using id of the merchant from dotenv)
  sample: Merchant Multi Media Semesta (id: 93f2e033-50ee-4b7c-a851-3f4d78847b25) has cityId equals to 153 (city.name: Jakarta Selatan).
}
*/
it('Get Merchant City ID from API', async function () {
  try {
    const merchantCityId = await getMerchantCity('93f2e033-50ee-4b7c-a851-3f4d78847b25');
    if (merchantCityId){
      console.log(merchantCityId);
      expect(merchantCityId).toBe(153);
    }
  } catch (error) {
    console.log('error on getting API: ' + error);
  }
});
