## Team E Front-End Mini Project
This project is used to Front-End App for Use Case APP B2B MarketPlace
Telkomsel Softdev Academy 2021

# Tobiko
Tobiko is e-commerce mobile app for telco industries.

## Team E Members
1. Hario Baskoro Basoeki (BE)
2. Agus Dwi Prayogo (D) (BE)
3. Revan Faredha (A) (BE)
4. Sugeng Dinda Winanjuar (BE)
5. I Wayan Gede Suarda (A) (BE)
6. Ayrtein Deliusca Dian Pratama (D) (BE)
7. Yudi Marta Arianto (A) (AND)
8. Irfan Saputra (D) (AND)
9. Raditya (A) (iOS)
10. Panji G Prasetya (D) (iOS)
11. Mochamad Rosyid Ridho (AND)

## Installation
Clone Tobiko Project and install all dependencies:
```sh
$ git clone "https://gitlab.com/tsel-marketplace-b2b/reactnative/tobiko.git"
$ cd tobiko
$ git checkout development
$ npm install
```

## Running Project
Start Metro Server in different terminal
```sh
$ npx react-native start
```

Android
```sh
$ npx react-native android
```

IOS
```sh
$ npx react-native ios
```

## Branch Naming Rules
Branch name format: feature/{feature-name}
```sh
$ git checkout -b feature/{feature-name}
$ git add .
$ git commit -m 'add your own comment/information related to code commit'
$ git push origin feature/{feature-name}
```

## Merge Request Flow
*feature/{BM No. from Jira}/{feature name} MR → development MR → production MR → master*


### Cheers! 🍻
Team E - Softdev Telkomsel